#ifndef COMMON_H
#define COMMON_H

#include <QString>



class UICommon
{
public:
    UICommon();

    static QString formatSize(qint64 size);
};

#endif // COMMON_H
