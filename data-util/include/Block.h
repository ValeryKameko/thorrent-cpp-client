#ifndef BLOCK_H
#define BLOCK_H

#include "BlockIdentifier.h"

#include <QtGlobal>
#include <QByteArray>

class Block
{
public:
    Block(qint64 pieceIndex, qint64 offset, const QByteArray& data);

    qint64 getPieceIndex() const;
    qint64 getOffset() const;
    const QByteArray &getData() const;

    BlockIdentifier getIdentifier() const;

private:
    qint64 pieceIndex;
    qint64 offset;
    QByteArray data;
};

#endif // BLOCK_H
