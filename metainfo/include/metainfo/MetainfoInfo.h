#ifndef METAINFOINFO_H
#define METAINFOINFO_H

#include <QList>

namespace metainfo {

class MetainfoInfoFileEntry {
public:
    qint64 fileSize;
    QByteArray md5Hash;
    QString relativePath;

    MetainfoInfoFileEntry(qint64 fileSize,
                          const QByteArray &md5Hash,
                          const QString &relativePath);
};

class MetainfoInfo
{
public:
    friend class MetainfoInfoBuilder;
    MetainfoInfo();

    const QList<QByteArray> &getPieceHashes() const;
    const QList<MetainfoInfoFileEntry> &getFileEntries() const;
    int getPieceLength() const;
    const QByteArray &getInfoData() const;
    const QString &getName() const;

private:
    QList<QByteArray> pieceHashes;
    QList<MetainfoInfoFileEntry> fileEntries;
    int pieceLength;
    QString name;

    QByteArray infoData;
};

class MetainfoInfoBuilder {
public:
    MetainfoInfoBuilder();
    ~MetainfoInfoBuilder();

    MetainfoInfoBuilder& setPieceLength(int pieceLength);
    MetainfoInfoBuilder& setPieceHashes(const QList<QByteArray> &pieceHashes);
    MetainfoInfoBuilder& setFileEntries(const QList<MetainfoInfoFileEntry> &fileEntries);
    MetainfoInfoBuilder& addFileEntry(const MetainfoInfoFileEntry &fileEntry);
    MetainfoInfoBuilder& setInfoData(const QByteArray &infoData);
    MetainfoInfoBuilder& setName(const QString &name);

    const QString& getName() const;

    void clear();
    MetainfoInfo build();

private:
    MetainfoInfo metainfoInfo;
};

}

#endif // METAINFOINFO_H
