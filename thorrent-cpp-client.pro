TEMPLATE = subdirs

SUBDIRS += \
    bencoding \
    data-util \
    metainfo \
    test/bencoding-test \
    test/metainfo-test \
    pwp-client \
    torrent-connection-manager \
    torrent-common \
    send-receive-controller \
    tracker-client \
    torrent-client \
    thorrent-gui

  CONFIG += ordered
