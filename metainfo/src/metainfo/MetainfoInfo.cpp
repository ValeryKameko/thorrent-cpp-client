#include "metainfo/MetainfoInfo.h"

#include <QDebug>

namespace metainfo {

MetainfoInfo::MetainfoInfo()
{

}

const QList<QByteArray> &MetainfoInfo::getPieceHashes() const
{
    return pieceHashes;
}

const QList<MetainfoInfoFileEntry> &MetainfoInfo::getFileEntries() const
{
    return fileEntries;
}

int MetainfoInfo::getPieceLength() const
{
    return pieceLength;
}

const QByteArray &MetainfoInfo::getInfoData() const
{
    return infoData;
}

const QString &MetainfoInfo::getName() const
{
    return name;
}

MetainfoInfoFileEntry::MetainfoInfoFileEntry(qint64 fileSize, const QByteArray &md5Hash, const QString &relativePath)
    : fileSize(fileSize), md5Hash(md5Hash), relativePath(relativePath)
{

}

MetainfoInfoBuilder::MetainfoInfoBuilder()
{
}

MetainfoInfoBuilder::~MetainfoInfoBuilder()
{
}

MetainfoInfoBuilder &MetainfoInfoBuilder::setPieceLength(int pieceLength)
{
    metainfoInfo.pieceLength = pieceLength;
    return *this;
}

MetainfoInfoBuilder &MetainfoInfoBuilder::setPieceHashes(const QList<QByteArray> &pieceHashes)
{
    metainfoInfo.pieceHashes = pieceHashes;
    return *this;
}

MetainfoInfoBuilder &MetainfoInfoBuilder::setFileEntries(const QList<MetainfoInfoFileEntry> &fileEntries)
{
    metainfoInfo.fileEntries = fileEntries;
    return *this;
}

MetainfoInfoBuilder &MetainfoInfoBuilder::addFileEntry(const MetainfoInfoFileEntry &fileEntry)
{
    metainfoInfo.fileEntries.append(fileEntry);
    return *this;
}

MetainfoInfoBuilder &MetainfoInfoBuilder::setInfoData(const QByteArray &infoData)
{
    metainfoInfo.infoData = infoData;
    return *this;
}

MetainfoInfoBuilder &MetainfoInfoBuilder::setName(const QString &name)
{
    metainfoInfo.name = name;
    return *this;
}

const QString &MetainfoInfoBuilder::getName() const
{
    return metainfoInfo.name;
}

void MetainfoInfoBuilder::clear()
{
    metainfoInfo = MetainfoInfo();
}

MetainfoInfo MetainfoInfoBuilder::build()
{
    return metainfoInfo;
}

}
