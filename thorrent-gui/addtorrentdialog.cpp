#include "addtorrentdialog.h"
#include "ui_addtorrentdialog.h"
#include "uicommon.h"

#include <QFileDialog>

#include <metainfo/Metainfo.h>
#include <metainfo/MetainfoParser.h>
#include <QtCore>


static const QString DEFAULT_STRING_VALUE = "<none>";
static const QString DEFAULT_INT_VALUE = "0";
static const QString DEFAULT_DATETIME_FORMAT = "hh:mm  dd.MM.yyyy";

AddTorrentDialog::AddTorrentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddTorrentDialog)
{
    ui->setupUi(this);
    isValidMetainfo = false;
    isValidDestination = false;
    clearTorrentInfo();
    updateOkButton();
}

AddTorrentDialog::~AddTorrentDialog()
{
    delete ui;
}

QString AddTorrentDialog::getDestinationFolder() const
{
    return destinationFolder;
}

void AddTorrentDialog::setDestinationFolder(const QString &value)
{
    destinationFolder = value;
}

QString AddTorrentDialog::getTorrentFilePath() const
{
    return torrentFilePath;
}

void AddTorrentDialog::setTorrentFilePath(const QString &value)
{
    torrentFilePath = value;
}

void AddTorrentDialog::clearTorrentInfo()
{
    ui->commentValueLabel->setText(DEFAULT_STRING_VALUE);
    ui->createdByValueLabel->setText(DEFAULT_STRING_VALUE);
    ui->totalSizeValueLabel->setText(DEFAULT_INT_VALUE);
    ui->creationDateValueLabel->setText(DEFAULT_STRING_VALUE);

    ui->announceTrackerValueLabel->setText("");
    ui->infoHashValueLabel->setText("");

    ui->announceTrackerListValueListWidget->clear();
    ui->contentValueTableWidget->setRowCount(1);
}

void AddTorrentDialog::updateTorrentInfo(const QString &filePath)
{
    torrentFilePath = filePath;
    clearTorrentInfo();

    isValidMetainfo = false;

    ui->torrentFileValueEdit->setText(filePath);
    if (filePath.isEmpty()) {
        updateOkButton();
        return;
    }

    QFile torrentFile(filePath);
    if (!torrentFile.open(QIODevice::ReadOnly)) {
        updateOkButton();
        return;
    }
    QScopedPointer<metainfo::Metainfo> metainfo(metainfo::MetainfoParser().parse(torrentFile.readAll()));
    torrentFile.close();

    if (metainfo.isNull()) {
        updateOkButton();
        return;
    }

    isValidMetainfo = true;
    updateTorrentInfo(*metainfo);
    updateOkButton();
}

void AddTorrentDialog::updateTorrentInfo(const metainfo::Metainfo &metainfo)
{
    if (!metainfo.getComment().isEmpty())
        ui->commentValueLabel->setText(metainfo.getComment());
    if (!metainfo.getCreatedBy().isEmpty())
        ui->createdByValueLabel->setText(metainfo.getCreatedBy());
    ui->creationDateValueLabel->setText(
                metainfo.getCreationDate().toString(DEFAULT_DATETIME_FORMAT));
    ui->announceTrackerValueLabel->setText(metainfo.getAnnounceTracker().toString());

    for (const QList<QUrl> &announceTrackerList : metainfo.getAnnounceTrackerList()) {
        for (const QUrl &announceTrackerUrl : announceTrackerList) {
            ui->announceTrackerListValueListWidget->addItem(announceTrackerUrl.toString());
        }
    }

    ui->infoHashValueLabel->setText(QCryptographicHash::hash(
                                   metainfo.getInfo().getInfoData(),
                                   QCryptographicHash::Sha1).toHex());

    qint64 totalSize = 0;

    int rowIndex = 0;
    ui->contentValueTableWidget->setRowCount(metainfo.getInfo().getFileEntries().size());
    for (const metainfo::MetainfoInfoFileEntry &entry : metainfo.getInfo().getFileEntries()) {
        ui->contentValueTableWidget->setItem(
                    rowIndex, 0, new QTableWidgetItem(entry.relativePath));
        ui->contentValueTableWidget->setItem(
                    rowIndex, 1, new QTableWidgetItem(UICommon::formatSize(entry.fileSize)));
        rowIndex++;
        totalSize += entry.fileSize;
    }
    ui->totalSizeValueLabel->setText(UICommon::formatSize(totalSize));
}

void AddTorrentDialog::updateDestinationFolder(const QString &destination)
{
    destinationFolder = destination;
    ui->destinationValueEdit->setText(destination);

    isValidDestination = !destinationFolder.isEmpty();

    updateOkButton();
}

void AddTorrentDialog::updateOkButton()
{
    ui->okButton->setEnabled(isValidMetainfo && isValidDestination);
}

void AddTorrentDialog::on_openTorrentFileButton_clicked()
{
    QString openTorrentFilePath = QFileDialog::getOpenFileName(
                this, "Select torrent file to open", "", "Torrent files (*.torrent);; All files (*.*)");
    if (openTorrentFilePath.isEmpty())
        return;

    updateTorrentInfo(openTorrentFilePath);
}

void AddTorrentDialog::on_openDestinationButton_clicked()
{
    QString openDestination = QFileDialog::getExistingDirectory(
                this, "Select desination folder");
    if (openDestination.isEmpty())
        return;

    updateDestinationFolder(openDestination);
}


void AddTorrentDialog::on_torrentFileValueEdit_textChanged(const QString &filePath)
{
    updateTorrentInfo(filePath);
}

metainfo::Metainfo AddTorrentDialog::getMetainfo() const
{
    return metainfo;
}

void AddTorrentDialog::setMetainfo(const metainfo::Metainfo &value)
{
    metainfo = value;
}
