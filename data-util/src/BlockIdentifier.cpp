#include "include/BlockIdentifier.h"

BlockIdentifier::BlockIdentifier()
{

}

BlockIdentifier::BlockIdentifier(qint64 pieceIndex, qint64 offset, qint64 length)
    : pieceIndex(pieceIndex), offset(offset), length(length)
{

}

BlockIdentifier::BlockIdentifier(const BlockIdentifier& other)
    : pieceIndex(other.pieceIndex), offset(other.offset), length(other.length)
{

}

qint64 BlockIdentifier::getPieceIndex() const
{
    return pieceIndex;
}

qint64 BlockIdentifier::getOffset() const
{
    return offset;
}

qint64 BlockIdentifier::getLength() const
{
    return length;
}

bool BlockIdentifier::operator==(const BlockIdentifier &other) const
{
    return pieceIndex == other.pieceIndex &&
            offset == other.offset &&
            length == other.length;
}
