#ifndef TORRENTGLOBALSTATE_H
#define TORRENTGLOBALSTATE_H

#include <QByteArray>



class TorrentGlobalState
{
public:
    static TorrentGlobalState &getInstance();

    QByteArray getClientId();

    int getListeningPort() const;

    int getMaxConnections() const;

private:
    TorrentGlobalState();

    QByteArray clientId;
    int listeningPort;
};

#endif // TORRENTGLOBALSTATE_H
