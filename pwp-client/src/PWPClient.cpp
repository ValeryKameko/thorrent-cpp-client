#include "include/PWPClient.h"

#include <QDataStream>
#include <QTimerEvent>
#include <QHostAddress>
#include <QtEndian>

static constexpr int PENDING_REQUEST_TIMEOUT = 60 * 1000;
static constexpr int MAX_PENDING_SIZE = 32 * 16384;
static constexpr int BUFFER_SIZE = 2048;
static constexpr int STATISTICS_UPDATE_INTERAL = 3 * 1000;
static constexpr int CLIENT_TIMEOUT = 120 * 1000;
static constexpr int INFO_HASH_SIZE = 20;
static constexpr int PEER_ID_SIZE = 20;
static constexpr int KEEP_ALIVE_INTERVAL = 30 * 1000;
static const QByteArray OPTIONS = QByteArray(8, '\0');
static const qint8 PROTOCOL_ID_SIZE = 19;
static const QByteArray PROTOCOL_ID = QString::fromUtf8("BitTorrent protocol").toUtf8().mid(0, PROTOCOL_ID_SIZE);
static constexpr int HEADER_MESSAGE_SIZE = 48;
static constexpr int CONNECT_TIMEOUT = 2 * 1000;
static constexpr int SAMPLES_COUNT = 10;

static QByteArray intToBigEndian(qint32 value) {
    QByteArray beData(4, '\0');

    qToBigEndian(value, beData.data());
    return beData;
}

static qint32 intFromBigEndian(const QByteArray &beData) {
    qint32 value = qFromBigEndian<qint32>(beData.data());
    return value;
}

PWPClient::PWPClient(QObject *parent)
    : QObject(parent)
{
    uploadSizeSamples << 0;
    downloadSizeSamples << 0;

    nextMessageLength = -1;

    QObject::connect(&socket, SIGNAL(readyRead()),
                     this, SIGNAL(readyToTransfer()));
    QObject::connect(&socket, SIGNAL(connected()),
                     this, SIGNAL(readyToTransfer()));

    QObject::connect(&socket, SIGNAL(connected()),
                     this, SIGNAL(connected()));
    QObject::connect(&socket, SIGNAL(disconnected()),
                     this, SIGNAL(disconnected()));
    QObject::connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)),
                     this, SIGNAL(socketError(QAbstractSocket::SocketError)));

    pendingRequestTimer = -1;
    keepAliveTimer = -1;
    timeoutTimer = startTimer(CONNECT_TIMEOUT);
    updateStatisticsTimer = startTimer(STATISTICS_UPDATE_INTERAL);

    qDebug() << "Created PWPClient";
}

void PWPClient::initialize(int piecesCount)
{
    state.getPeerPieces().resize(piecesCount);
    if (!state.getSentHandshake())
        sendHandshake();
}

const QTcpSocket &PWPClient::getSocket() const
{
    return socket;
}

QTcpSocket &PWPClient::getSocket()
{
    return socket;
}

const PWPConnectionState &PWPClient::getState() const
{
    return state;
}

PWPConnectionState &PWPClient::getState()
{
    return state;
}

void PWPClient::sendChoke()
{
    qDebug() << "Send choke"
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(1));
    message.append(quint8(ChokeMessage));

    writeData(message.constData(), message.size());
    state.setOther(state.getOther() | PWPConnectionState::Choked);
    state.getPendingBlocks().clear();
    state.setPendingBlocksSize(0);
}

void PWPClient::sendUnchoke()
{
    qDebug() << "Send unchoke"
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(1));
    message.append(quint8(UnchokeMessage));

    writeData(message.constData(), message.size());
    state.setOther(state.getOther() & ~PWPConnectionState::Choked);

    if (pendingRequestTimer != -1) {
        killTimer(pendingRequestTimer);
        pendingRequestTimer = -1;
    }
}

void PWPClient::sendInterested()
{
    qDebug() << "Send interested"
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(1));
    message.append(quint8(InterestedMessage));

    writeData(message.constData(), message.size());
    state.setSelf(state.getSelf() | PWPConnectionState::Interested);

    if (pendingRequestTimer != -1) {
        killTimer(pendingRequestTimer);
        pendingRequestTimer = -1;
    }
    pendingRequestTimer = startTimer(PENDING_REQUEST_TIMEOUT);
}

void PWPClient::sendKeepAlive()
{
    qDebug() << "Send Keep Alive"
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(0));

    writeData(message.constData(), message.size());
}

void PWPClient::sendNotInterested()
{
    qDebug() << "Send not interested"
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(1));
    message.append(quint8(NotInterestedMessage));

    writeData(message.constData(), message.size());
    state.setSelf(state.getSelf() & ~PWPConnectionState::Interested);
}

void PWPClient::sendHave(int piece)
{
    qDebug() << "Send have " << piece
             << " of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(5));
    message.append(quint8(HaveMessage));
    message.append(intToBigEndian(piece));

    writeData(message.constData(), message.size());
}

void PWPClient::sendBitfield(const QBitArray &bitfield)
{
    if (!state.getSentHandshake())
        sendHandshake();

    if (bitfield.count(true) == 0) {
        qDebug() << "Not sending bitfield"
                 << " of peer " << state.getOtherPeerId() << "";
        return;
    }

    int bytesCount = (bitfield.size() + 7) / 8;

    QByteArray message;
    message.append(intToBigEndian(bytesCount + 1));
    message.append(quint8(BitfieldMessage));

    for (int byteIndex = 0; byteIndex < bytesCount; byteIndex++) {
        quint8 bt = 0;
        for (int bitIndex = 0; bitIndex < 8; bitIndex++) {
            int reverseBitIndex = 7 - bitIndex;
            int bitOffset = byteIndex * 8 + bitIndex;
            if (bitOffset >= bitfield.size())
                continue;
            if (bitfield.testBit(bitOffset))
                bt |= (1 << reverseBitIndex);
        }
        message.append(bt);
    }

    qDebug() << "Sending bitfield"
             << " of peer " << state.getOtherPeerId() << "";

    writeData(message.constData(), message.size());
}

void PWPClient::sendRequest(const BlockIdentifier &identifier)
{
    qDebug() << "Sending request block(" << identifier.getPieceIndex()
             <<  ", " << identifier.getOffset()
             <<  ", " << identifier.getLength()
             << ") of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(13));
    message.append(quint8(RequestMessage));
    message.append(intToBigEndian(identifier.getPieceIndex()));
    message.append(intToBigEndian(identifier.getOffset()));
    message.append(intToBigEndian(identifier.getLength()));

    writeData(message.constData(), message.size());
    state.getIncomingBlockRequests() << identifier;

    if (pendingRequestTimer != -1) {
        killTimer(pendingRequestTimer);
        pendingRequestTimer = -1;
    }
    pendingRequestTimer = startTimer(PENDING_REQUEST_TIMEOUT);
}

void PWPClient::sendCancel(const BlockIdentifier &identifier)
{
    qDebug() << "Sending cancel block(" << identifier.getPieceIndex()
             <<  ", " << identifier.getOffset()
             <<  ", " << identifier.getLength()
             << ") of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(13));
    message.append(quint8(CancelMessage));
    message.append(intToBigEndian(identifier.getPieceIndex()));
    message.append(intToBigEndian(identifier.getOffset()));
    message.append(intToBigEndian(identifier.getLength()));

    writeData(message.constData(), message.size());
    state.getIncomingBlockRequests().removeAll(identifier);
}

void PWPClient::sendPiece(const Block &block)
{
    qDebug() << "Sending block(" << block.getIdentifier().getPieceIndex()
             <<  ", " << block.getIdentifier().getOffset()
             <<  ", " << block.getIdentifier().getLength()
             << ") of peer " << state.getOtherPeerId() << "";

    QByteArray message;
    message.append(intToBigEndian(9 + block.getIdentifier().getLength()));
    message.append(quint8(PieceMessage));
    message.append(intToBigEndian(block.getIdentifier().getPieceIndex()));
    message.append(intToBigEndian(block.getIdentifier().getOffset()));
    message.append(block.getData());

    BlockInfo info;
    info.block = message;
    info.length = block.getIdentifier().getLength();
    info.offset = block.getIdentifier().getOffset();
    info.pieceIndex = block.getIdentifier().getPieceIndex();

    state.getPendingBlocks().enqueue(info);
    state.setPendingBlocksSize(state.getPendingBlocksSize() + info.block.size());
    if (state.getPendingBlocksSize() > MAX_PENDING_SIZE) {
        qDebug() << "Too many data choke/unchoke"
                 << " of peer " << state.getOtherPeerId() << "";
        sendChoke();
        sendUnchoke();
        return;
    }
    emit readyToTransfer();
}

qint64 PWPClient::writeToSocket(qint64 count)
{
    qint64 writtenCount = 0;
    do {
        if (outgoingBuffer.isEmpty() && !state.getPendingBlocks().isEmpty()) {
            BlockInfo info = state.getPendingBlocks().dequeue();
            state.setPendingBlocksSize(state.getPendingBlocksSize() - info.block.size());
            outgoingBuffer += info.block;
        }

        qint64 currentWritten = socket.write(outgoingBuffer.data(), count);
        if (currentWritten <= 0) {
            if (writtenCount > 0)
                emit bytesSent(writtenCount);
            return (writtenCount == 0) ? currentWritten : writtenCount;
        }

        writtenCount += currentWritten;
        uploadSizeSamples.first() += currentWritten;
        outgoingBuffer.remove(0, currentWritten);
    } while (writtenCount < count && !(outgoingBuffer.isEmpty() && state.getPendingBlocks().isEmpty()));

    if (writtenCount > 0)
        emit bytesSent(writtenCount);

    return writtenCount;
}

qint64 PWPClient::readFromSocket(qint64 count)
{
    char buffer[BUFFER_SIZE];

    qint64 readCount = 0;
    do {
        qint64 currentRead = socket.read(buffer, qMin<qint64>(BUFFER_SIZE, count - readCount));
        if (currentRead <= 0)
            break;

        readCount += currentRead;

        qint64 offset = incomingBuffer.size();
        incomingBuffer.resize(incomingBuffer.size() + currentRead);
        memcpy(incomingBuffer.data() + offset, buffer, currentRead);
    } while (readCount < count);

    downloadSizeSamples.first() += readCount;

    if (readCount > 0) {
        emit bytesReceived(readCount);
        processIncomingMessages();
    }
    return readCount;
}

qint64 PWPClient::getDownloadSpeed() const
{
    qint64 sum = 0;
    for (auto element : downloadSizeSamples) {
        sum += element;
    }
    return sum / (STATISTICS_UPDATE_INTERAL / 1000 * downloadSizeSamples.size());
}

qint64 PWPClient::getUploadSpeed() const
{
    qint64 sum = 0;
    for (auto element : uploadSizeSamples) {
        sum += element;
    }
    return sum / (STATISTICS_UPDATE_INTERAL / 1000 * uploadSizeSamples.size());
}

bool PWPClient::canTransfer() const
{
    return
            socket.bytesAvailable() > 0 ||
            !outgoingBuffer.isEmpty() ||
            !state.getPendingBlocks().isEmpty();
}

qint64 PWPClient::getSocketBytesAvailable() const
{
    return socket.bytesAvailable();
}

qint64 PWPClient::getBytesAvailable() const
{
    return incomingBuffer.size();
}

qint64 PWPClient::getBytesToWrite() const
{
    return outgoingBuffer.size();
}

void PWPClient::connectToHost(const QHostAddress &address, quint16 port, QIODevice::OpenMode openMode)
{
    socket.connectToHost(address, port, openMode);
    qDebug() << "Connecting to" << address << " : " << port
             << " peer "  << socket.peerAddress() << " : " << socket.peerPort();
}

void PWPClient::diconnectFromHost()
{
    qDebug() << "Disconnecting from" << socket.peerAddress() << " : " << socket.peerPort()
             << " peer "  << socket.peerAddress() << " : " << socket.peerPort();
    socket.disconnectFromHost();
}

void PWPClient::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timeoutTimer) {
        if (invalidateTimeout) {
            invalidateTimeout = false;
        } else {
            qDebug() << "Abort peer due to timeout "
                     << " of peer "  << socket.peerAddress() << " : " << socket.peerPort();
            socket.disconnectFromHost();
            emit connectionTimeout();
        }
    } else if (event->timerId() == keepAliveTimer) {
        sendKeepAlive();
    } else if (event->timerId() == pendingRequestTimer) {
        qDebug() << "Abort peer due to pendingRequestTimer "
                 << " of peer " << socket.peerAddress() << " : " << socket.peerPort();
        socket.disconnectFromHost();
        emit pendingRequestTimeout();

    } else if (event->timerId() == updateStatisticsTimer) {
        uploadSizeSamples << 0;
        downloadSizeSamples << 0;
        if (uploadSizeSamples.length() >= SAMPLES_COUNT)
            uploadSizeSamples.dequeue();
        if (downloadSizeSamples.length() >= SAMPLES_COUNT)
            downloadSizeSamples.dequeue();
    }
    QObject::timerEvent(event);
}

qint64 PWPClient::writeData(const char *data, qint64 size)
{
    qint64 offset = outgoingBuffer.size();
    outgoingBuffer.resize(size + offset);
    memcpy(outgoingBuffer.data() + offset, data, size);
    emit readyToTransfer();
    return size;
}

qint64 PWPClient::readData(char *data, qint64 size)
{
    qint64 readSize = qMin<qint64>(size, incomingBuffer.size());
    memcpy(data, incomingBuffer.data(), readSize);
    incomingBuffer.remove(0, readSize);
    emit readyToTransfer();
    return readSize;
}

QByteArray PWPClient::read(qint64 size)
{
    QByteArray data(size, 0);
    qint64 readSize = readData(data.data(), size);
    data.resize(readSize);
    return data;
}

void PWPClient::sendHandshake()
{
    if (state.getSentHandshake())
        return;

    state.setSentHandshake(true);

    if (timeoutTimer != -1) {
        killTimer(timeoutTimer);
        timeoutTimer = -1;
    }
    timeoutTimer = startTimer(CLIENT_TIMEOUT);

    qDebug() << "Send handshake "
             << " of peer " << socket.peerAddress() << " : " << socket.peerPort() << "";

    QByteArray message;
    message.append(PROTOCOL_ID_SIZE);
    message.append(PROTOCOL_ID);
    message.append(OPTIONS);
    message.append(state.getInfoHash());
    message.append(state.getSelfPeerId());

    writeData(message.constData(), message.size());
}

void PWPClient::processIncomingMessages()
{
    invalidateTimeout = true;
    if (!state.getRecevedHandshake()) {
        if (getBytesAvailable() < HEADER_MESSAGE_SIZE)
            return;
        QByteArray protocolId = read(PROTOCOL_ID_SIZE + 1);
        if (protocolId.mid(1) != PROTOCOL_ID || protocolId.at(0) != PROTOCOL_ID_SIZE) {
            socket.disconnectFromHost();
            qDebug() << "Abort peer due wrong header format"
                     << " of peer " << socket.peerAddress() << " : " << socket.peerPort() << "";
            return;
        }

        QByteArray options = read(OPTIONS.length());

        QByteArray infoHash = read(INFO_HASH_SIZE);
        if (!state.getInfoHash().isEmpty() && state.getInfoHash() != infoHash) {
            socket.abort();
            qDebug() << "Abort peer due wrong header format"
                     << " of peer " << socket.peerAddress() << " : " << socket.peerPort() << "";
            return;
        }

        emit infoHashReceived(infoHash);

        if (state.getInfoHash().isEmpty()) {
            socket.abort();
            qDebug() << "Abort peer due wrong header format"
                     << " of peer " << socket.peerAddress() << " : " << socket.peerPort() << "";
            return;
        }
        if (!state.getSentHandshake())
            sendHandshake();

        state.setRecevedHandshake(true);
        qDebug() << "Handshake received "
                 << " of peer " << socket.peerAddress() << " : " << socket.peerPort() << "";
    }

    if (!state.getGotPeerId()) {
        if (getBytesAvailable() < PEER_ID_SIZE)
            return;
        QByteArray peerId = read(PEER_ID_SIZE);
        state.setGotPeerId(true);
        state.setOtherPeerId(peerId);
        if (state.getOtherPeerId() == state.getSelfPeerId()) {
            socket.abort();
            qDebug() << "Abort peer due connecting to us"
                     << " of peer " << state.getOtherPeerId() << "";

            return;
        }

        qDebug() << "Peer id received"
                 << " of peer " << state.getOtherPeerId() << "";
    }

    if (!keepAliveTimer)
        keepAliveTimer = startTimer(KEEP_ALIVE_INTERVAL);

    do {
        if (nextMessageLength == -1) {
            if (getBytesAvailable() < 4)
                return;
            QByteArray messageSizeData = read(4);
            quint32 messageSize = intFromBigEndian(messageSizeData);

            nextMessageLength = messageSize;
        }

        if (nextMessageLength == 0) {
            nextMessageLength = -1;
            break;
        }

        if (getBytesAvailable() < nextMessageLength)
            return;
        qDebug() << "received message " << nextMessageLength
                 << " of peer " << state.getOtherPeerId() << "";

        QByteArray messageData = read(nextMessageLength);
        nextMessageLength = -1;

        if (!processMessage(messageData))
            break;
    } while (getBytesAvailable() > 0);
}

bool PWPClient::processMessage(const QByteArray &message)
{
    if (message.size() == 0)
        return true;
    quint8 messageTypeByte = message[0];

    MessageType messageType = static_cast<MessageType>(messageTypeByte);

    qint32 offset;
    qint32 length;
    QByteArray data;
    qint32 pieceIndex;
    BlockIdentifier identifier;

    qDebug() << "New message " << message.size()
             << " from peer " << state.getOtherPeerId() << "";


    switch (messageType) {
    case ChokeMessage:
        state.setSelf(state.getSelf() | PWPConnectionState::Choked);
        state.getIncomingBlockRequests().clear();

        if (pendingRequestTimer != -1) {
            killTimer(pendingRequestTimer);
            pendingRequestTimer = -1;
        }

        qDebug() << "Choke from "
                 << " peer " << state.getOtherPeerId() << "";
        emit chokedReceived();
        break;
    case UnchokeMessage:
        state.setSelf(state.getSelf() & ~PWPConnectionState::Choked);

        qDebug() << "Unchoke from "
                 << " peer " << state.getOtherPeerId() << "";
        emit unchokedReceived();
        break;
    case InterestedMessage:
        state.setOther(state.getOther() | PWPConnectionState::Interested);

        qDebug() << "Interested from "
                 << " peer " << state.getOtherPeerId() << "";
        emit interestedReceived();
        break;
    case NotInterestedMessage:
        state.setOther(state.getOther() & ~PWPConnectionState::Interested);

        qDebug() << "Not interested from "
                 << " peer " << state.getOtherPeerId() << "";
        emit notInterestedReceived();
        break;
    case HaveMessage:
        if (message.size() != 5)
            return true;
        pieceIndex = intFromBigEndian(message.mid(1, 4));
        if (pieceIndex < state.getPeerPieces().size()) {
            state.getPeerPieces().setBit(pieceIndex);
        }

        qDebug() << "Have " << pieceIndex << "from "
                 << " peer " << state.getOtherPeerId() << "";
        emit bitfieldReceived(state.getPeerPieces());
        break;
    case BitfieldMessage:
        data = message.mid(1);

        for (int byteIndex = 0; byteIndex < data.size(); byteIndex++) {
            for (int bitIndex = 0; bitIndex < 8; bitIndex++) {
                int bitReversedIndex = 7 - bitIndex;
                int bitOffset = byteIndex * 8 + bitIndex;
                if (bitOffset >= state.getPeerPieces().size())
                    continue;
                bool bitValue = (data.at(byteIndex) & (1 << bitReversedIndex)) != 0;
                state.getPeerPieces().setBit(bitOffset, bitValue);
            }
        }

        qDebug() << "Bitfield from "
                 << " peer " << state.getOtherPeerId() << "";

        emit bitfieldReceived(state.getPeerPieces());
        break;
    case PieceMessage:
        if (message.size() < 9)
            return true;
        pieceIndex = intFromBigEndian(message.mid(1, 4));
        offset = intFromBigEndian(message.mid(5, 4));
        data = message.mid(9);

        state.getIncomingBlockRequests()
                .removeAll(BlockIdentifier(pieceIndex, offset, data.size()));

        qDebug() << "Block (" << pieceIndex
                 << ", " << offset
                 << ", " << data.size()
                 << ") from "
                 << " peer " << state.getOtherPeerId() << "";

        emit pieceReceived(Block(pieceIndex, offset, data));

        if (pendingRequestTimer != -1) {
            killTimer(pendingRequestTimer);
            pendingRequestTimer = 0;
        }
        break;
    case RequestMessage:
        if (message.size() != 13)
            return true;
        pieceIndex = intFromBigEndian(message.mid(1, 4));
        offset = intFromBigEndian(message.mid(5, 4));
        length = intFromBigEndian(message.mid(9, 4));

        qDebug() << "Request (" << pieceIndex
                 << ", " << offset
                 << ", " << length
                 << ") from "
                 << " peer " << state.getOtherPeerId() << "";

        emit requestReceived(BlockIdentifier(pieceIndex, offset, length));
        break;
    case CancelMessage:
        if (message.size() != 13)
            return true;
        pieceIndex = intFromBigEndian(message.mid(1, 4));
        offset = intFromBigEndian(message.mid(5, 4));
        length = intFromBigEndian(message.mid(9, 4));

        qDebug() << "Cancel (" << pieceIndex
                 << ", " << offset
                 << ", " << length
                 << ") from "
                 << " peer " << state.getOtherPeerId() << "";

        identifier = BlockIdentifier(pieceIndex, offset, length);
        for (auto it = state.getPendingBlocks().begin();
             it != state.getPendingBlocks().end();) {
            if (identifier == BlockIdentifier(it->pieceIndex, it->offset, it->length)) {
                it = state.getPendingBlocks().erase(it);
            } else {
                it++;
            }
        }
        break;
    default:
        qDebug() << "Unknown messages from "
                 << " peer " << state.getOtherPeerId() << "";
    }
    return true;
}
