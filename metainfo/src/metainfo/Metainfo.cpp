#include "metainfo/Metainfo.h"

namespace metainfo {

Metainfo::Metainfo()
{
    isPrivate = false;
}

Metainfo::~Metainfo()
{
}

const QDateTime &Metainfo::getCreationDate() const
{
    return creationDate;
}

const QUrl &Metainfo::getAnnounceTracker() const
{
    return announceTracker;
}

const QList<QList<QUrl> > &Metainfo::getAnnounceTrackerList() const
{
    return announceTrackerList;
}

const QString &Metainfo::getComment() const
{
    return comment;
}

const QString &Metainfo::getCreatedBy() const
{
    return createdBy;
}

const QList<QUrl> &Metainfo::getUrlList() const
{
    return urlList;
}

bool Metainfo::getPrivate() const
{
    return isPrivate;
}

const MetainfoInfo &Metainfo::getInfo() const
{
    return info;
}

MetainfoBuilder::MetainfoBuilder()
{
}

MetainfoBuilder::~MetainfoBuilder()
{
}

MetainfoBuilder &MetainfoBuilder::setPrivate(bool isPrivate)
{
    metainfo.isPrivate = isPrivate;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setCreationDate(const QDateTime &creationDate)
{
    metainfo.creationDate = creationDate;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setCreatedBy(const QString &createdBy)
{
    metainfo.createdBy = createdBy;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setComment(const QString &comment)
{
    metainfo.comment = comment;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setAnnounceTracker(const QUrl &announceTracker)
{
    metainfo.announceTracker = announceTracker;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setAnnounceTrackerList(const QList<QList<QUrl> > &announceTrackerList)
{
    metainfo.announceTrackerList = announceTrackerList;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::addAnnounceTrackerGroup(const QList<QUrl> &announceTrackerGroup)
{
    metainfo.announceTrackerList.append(announceTrackerGroup);
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setUrlList(const QList<QUrl> &urlList)
{
    metainfo.urlList = urlList;
    return *this;
}

MetainfoBuilder &MetainfoBuilder::addUrl(const QUrl &url)
{
    metainfo.urlList.append(url);
    return *this;
}

MetainfoBuilder &MetainfoBuilder::setInfo(const MetainfoInfo &info)
{
    metainfo.info = info;
    return *this;
}

void MetainfoBuilder::clear()
{
   metainfo = Metainfo();
}

Metainfo MetainfoBuilder::build()
{
    return metainfo;
}


}
