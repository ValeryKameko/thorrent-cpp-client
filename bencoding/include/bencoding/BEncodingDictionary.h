#ifndef BENCODINGDICTIONARY_H
#define BENCODINGDICTIONARY_H

#include "BEncodingObject.h"
#include "BEncodingString.h"

#include <QMap>

namespace bencoding {

class BEncodingDictionary : public BEncodingObject
{
public:
    BEncodingDictionary(const QMap<BEncodingString, BEncodingObject *> &map);
    BEncodingDictionary();
    virtual ~BEncodingDictionary() override;

    BEncodingObject * deepCopy() const override;

    virtual QByteArray encodeToByteArray() const override;

    const QMap<BEncodingString, BEncodingObject *> &getMap() const;
    QMap<BEncodingString, BEncodingObject *> &getMap();
private:
    QMap<BEncodingString, BEncodingObject *> map;
};

}

#endif // BENCODINGDICTIONARY_H
