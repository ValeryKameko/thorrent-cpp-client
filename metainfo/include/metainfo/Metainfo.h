#ifndef METAINFO_H
#define METAINFO_H

#include<QUrl>
#include<QByteArray>
#include<QDateTime>

#include "MetainfoInfo.h"

namespace metainfo {

class Metainfo
{

public:
    friend class MetainfoBuilder;
    Metainfo();
    ~Metainfo();

    const QDateTime &getCreationDate() const;
    const QUrl &getAnnounceTracker() const;
    const QList<QList<QUrl>> &getAnnounceTrackerList() const;
    const QString &getComment() const;
    const QString &getCreatedBy() const;
    const QList<QUrl> &getUrlList() const;
    bool getPrivate() const;

    const MetainfoInfo &getInfo() const;

private:
    QDateTime creationDate;
    QUrl announceTracker;
    QList<QList<QUrl>> announceTrackerList;
    QString comment;
    QString createdBy;
    QList<QUrl> urlList;
    bool isPrivate;

    MetainfoInfo info;
};

class MetainfoBuilder {
public:
    MetainfoBuilder();
    ~MetainfoBuilder();

    MetainfoBuilder& setPrivate(bool isPrivate);
    MetainfoBuilder& setCreationDate(const QDateTime &creationDate);
    MetainfoBuilder& setCreatedBy(const QString &createdBy);
    MetainfoBuilder& setComment(const QString &comment);
    MetainfoBuilder& setAnnounceTracker(const QUrl &announceTracker);
    MetainfoBuilder& setAnnounceTrackerList(const QList<QList<QUrl>> &announceTrackerList);
    MetainfoBuilder& addAnnounceTrackerGroup(const QList<QUrl> &announceTrackerGroup);
    MetainfoBuilder& setUrlList(const QList<QUrl> &urlList);
    MetainfoBuilder& addUrl(const QUrl &url);
    MetainfoBuilder& setInfo(const MetainfoInfo &info);

    void clear();
    Metainfo build();

private:
    Metainfo metainfo;
};

}

#endif // METAINFO_H
