#ifndef BENCODINGNUMBER_H
#define BENCODINGNUMBER_H

#include "BEncodingObject.h"
#include <QObject>


namespace bencoding {

class BEncodingNumber : public BEncodingObject
{
public:
    BEncodingNumber(qint64 value);
    BEncodingNumber(int value);
    BEncodingNumber(qint16 value);
    BEncodingNumber(const BEncodingNumber &other);
    BEncodingNumber();


    BEncodingObject * deepCopy() const override;

    virtual ~BEncodingNumber() override;

    virtual QByteArray encodeToByteArray() const override;

    qint64 getInt64Value() const;
    qint16 getInt16Value() const;
    int getIntValue() const;

    void setInt64Value(qint64 value);
    void setInt16Value(qint16 value);
    void setIntValue(int value);

private:
    qint64 value;
};

}

#endif // BENCODINGNUMBER_H
