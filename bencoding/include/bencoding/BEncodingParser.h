#ifndef BENCODINGPARSER_H
#define BENCODINGPARSER_H

#include "BEncodingDictionary.h"
#include "BEncodingList.h"
#include "BEncodingNumber.h"
#include "BEncodingObject.h"
#include "BEncodingString.h"

namespace bencoding {

class BEncodingParser
{
public:
    BEncodingParser();

    BEncodingObject *parse(const QByteArray &byteArray);
    BEncodingDictionary *parseDictionary(const QByteArray &byteArray);
    BEncodingList *parseList(const QByteArray &byteArray);
    BEncodingNumber *parseNumber(const QByteArray &byteArray);
    BEncodingString *parseString(const QByteArray &byteArray);
private:
    BEncodingObject *parse();
    BEncodingDictionary *parseDictionary();
    BEncodingList *parseList();
    BEncodingNumber *parseNumber();
    BEncodingString *parseString();

    int pointer;
    QByteArray byteArray;
};

}

#endif // BENCODINGPARSER_H
