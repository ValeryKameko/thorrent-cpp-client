#include "include/TorrentClientState.h"

#include <QDebug>

TorrentClientState::TorrentClientState()
    : infoHash(QByteArray()), downloadedSize(0),
      uploadedSize(0), pieceCount(0), state(IdleState)
{

}

QByteArray TorrentClientState::getInfoHash() const
{
    return infoHash;
}

void TorrentClientState::setInfoHash(const QByteArray &value)
{
    qDebug() << "Set infohash " << value.toHex()
             << " TorrentClientState " << getMetainfo().getInfo().getName() << "";

    infoHash = value;
}

qint64 TorrentClientState::getDownloadedSize() const
{
    return downloadedSize;
}

void TorrentClientState::setDownloadedSize(const qint64 &value)
{
    downloadedSize = value;
}

qint64 TorrentClientState::getUploadedSize() const
{
    return uploadedSize;
}

void TorrentClientState::setUploadedSize(const qint64 &value)
{
    uploadedSize = value;
}

TorrentClientState::TorrentClientWorkingState TorrentClientState::getState() const
{
    return state;
}

void TorrentClientState::setState(const TorrentClientWorkingState &value)
{
    state = value;
    switch (state) {
    case TorrentClientState::IdleState:
        stateString = "Idle";
        break;
    case TorrentClientState::PausedState:
        stateString = "Paused";
        break;
    case TorrentClientState::StoppingState:
        stateString = "Stopping";
        break;
    case TorrentClientState::InitializationState:
        stateString = "Initialization";
        break;
    case TorrentClientState::SearchingState:
        stateString = "Searching";
        break;
    case TorrentClientState::ConnectingState:
        stateString = "Connecting";
        break;
    case TorrentClientState::DownloadingState:
        stateString = "Downloading";
        break;
    case TorrentClientState::CompleteState:
        stateString = "Complete";
        break;
    }

}

qint64 TorrentClientState::getTotalSize() const
{
    return totalSize;
}

void TorrentClientState::setTotalSize(const qint64 &value)
{
    totalSize = value;
}

QString TorrentClientState::getErrorString() const
{
    return errorString;
}

void TorrentClientState::setErrorString(const QString &value)
{
    errorString = value;
}

TorrentClientState::TorrentClientError TorrentClientState::getError() const
{
    return error;
}

void TorrentClientState::setError(const TorrentClientError &value)
{
    error = value;
    switch (value) {
    case TorrentClientError::NoError:
        errorString = "";
        break;
    case TorrentClientError::UnknownError:
        errorString = "Unknown error";
        break;
    case TorrentClientError::MetainfoParsingError:
        errorString = "Invalid torrent data";
        break;
    case TorrentClientError::TrackerError:
        errorString = "Unable to connect to tracker";
        break;
    case TorrentClientError::FilesystemError:
        errorString = "File error";
        break;
    case TorrentClientError::TorrentServerError:
        errorString = "Unable to initialize server";
        break;
    }
}

metainfo::Metainfo &TorrentClientState::getMetainfo()
{
    return metainfo;
}

QString TorrentClientState::getStorageFolder() const
{
    return storageFolder;
}

void TorrentClientState::setStorageFolder(const QString &value)
{
    storageFolder = value;
}

QString TorrentClientState::getStateString() const
{
    return stateString;
}

void TorrentClientState::setStateString(const QString &value)
{
    stateString = value;
}

int TorrentClientState::getPieceCount() const
{
    return pieceCount;
}

void TorrentClientState::setPieceCount(int value)
{
    pieceCount = value;
}
