#include "include/TorrentClient.h"

#include <FileStorage.h>
#include <FileStorageWorker.h>
#include <HttpTrackerClient.h>
#include <PWPClient.h>
#include <QTimer>
#include <TorrentClientState.h>
#include <TorrentConnectionManager.h>
#include <TorrentGlobalState.h>
#include <QRandomGenerator>
#include <QTimerEvent>
#include <SendReceiveController.h>

#include <metainfo/MetainfoParser.h>

using namespace metainfo;

static const int RATE_CONTROL_WINDOW_LENGTH = 10;
static const int CONNECT_TO_PEERS_DELAY = 5 * 1000;
static const int STATISTICS_UPDATE_INTEVAL = 1000;
static const int MAX_SPEED_COUNT = 4;
static const int UPLOAD_SCHEDULING_INTERVAL = 10 * 1000;
static const int MAX_PENDING_BLOCK_COUNT = 5;
static const int BLOCK_SIZE = 1 << 14;
static const int ALIVE_SPEED_LIMIT = 1024;
static const int MAX_CONNECTIONS_PER_CLIENT = 10;
static const int FAST_END_LEVEL = 10;

class TorrentPiece {
public:
    int pieceIndex;
    int length;

    QBitArray completedBlocks;
    QBitArray requestedBlocks;

    bool isInProgress;
};

class TorrentClientPrivate
{
public:
    TorrentClientPrivate(TorrentClient *client);

    void setError(TorrentClientState::TorrentClientError error);
    void setState(TorrentClientState::TorrentClientWorkingState state);

    void completeTorrent();

    QSharedPointer<HttpTrackerClient> trackerClient;
    FileStorageWorker fileManager;

    QList<PWPClient *> connections;
    QList<TorrentPeer *> peers;

    bool isSchedulerCalled;
    void callScheduler();

    bool isConnectingToClients;
    void callPeerConnector();

    int uploadScheduleTimer;
    bool isPaused;

    QMap<int, PWPClient *> readRequests;
    QMultiMap<PWPClient *, TorrentPiece *> payloads;
    QMap<int, TorrentPiece *> pendingPieces;
    QBitArray completedPieces;
    QBitArray incompletePieces;

    int lastProgressValue;
    qint64 downloadedBytes;
    qint64 uploadedBytes;
    QQueue<int> downloadRateSamples;
    QQueue<int> uploadRateSamples;
    int statisticsUpdateTimer;

    TorrentClient *client;
};

TorrentClientPrivate::TorrentClientPrivate(TorrentClient *client)
    : trackerClient(new HttpTrackerClient(client)), client(client)
{
    isPaused = false;

    trackerClient->setTorrentClientState(&client->state);
    trackerClient->setTorrentGlobalState(&TorrentGlobalState::getInstance());

    client->getState().setError(TorrentClientState::NoError);
    client->getState().setState(TorrentClientState::IdleState);
    client->getState().setStateString("Idle");
    isSchedulerCalled = false;
    isConnectingToClients = false;
    uploadScheduleTimer = 0;
    lastProgressValue = -1;

    downloadRateSamples.enqueue(0);
    uploadRateSamples.enqueue(0);
    statisticsUpdateTimer = -1;

    qDebug() << "Initialize "
             << "TorrentClient";
}

void TorrentClientPrivate::setError(TorrentClientState::TorrentClientError error)
{
    client->getState().setError(error);
    if (error != TorrentClientState::NoError) {
        qDebug() << "Error " << client->getState().getErrorString()
                 << " TorrentClient " << client->getState().getMetainfo().getInfo().getName() << "";

        emit client->errorFired(error);
    }
}

void TorrentClientPrivate::setState(TorrentClientState::TorrentClientWorkingState state)
{
    client->getState().setState(state);
    qDebug() << "State changed " << client->getState().getStateString()
             << " TorrentClient " << client->getState().getMetainfo().getInfo().getName() << "";
    emit client->stateChanged(state);
}

void TorrentClientPrivate::completeTorrent()
{
    QTimer::singleShot(0, client, &TorrentClient::stop);

    qDebug() << "Torrent completed"
             << " TorrentClient " << client->getState().getMetainfo().getInfo().getName();
    return;
}

void TorrentClientPrivate::callScheduler()
{
    if (!isSchedulerCalled) {
        isSchedulerCalled = true;
        qDebug() << "Schedule scheduler"
                 << " TorrentClient " << client->getState().getMetainfo().getInfo().getName() << "";
        QMetaObject::invokeMethod(client, "scheduleDownloads", Qt::QueuedConnection);
    }
}

void TorrentClientPrivate::callPeerConnector()
{
    if (!isConnectingToClients) {
        isConnectingToClients = true;
        qDebug() << "Schedule peer connector"
                 << " TorrentClient " << client->getState().getMetainfo().getInfo().getName() << "";
        QTimer::singleShot(CONNECT_TO_PEERS_DELAY, client, SLOT(connectToPeers()));
    }
}

TorrentClient::TorrentClient(QObject *parent)
    : QObject(parent),
      impl(new TorrentClientPrivate(this))
{
    connect(&impl->fileManager, SIGNAL(dataRead(int,Block)),
            this, SLOT(sendBlockToPeer(int,Block)));
    connect(&impl->fileManager, SIGNAL(fullVerificationProgress(int, int)),
            this, SLOT(updateProgress(int, int)));
    connect(&impl->fileManager, SIGNAL(fullVerificationDone(int)),
            this, SLOT(fullVerificationDone(int)));
    connect(&impl->fileManager, SIGNAL(pieceVerified(int, qint64, bool)),
            this, SLOT(pieceVerified(int, qint64, bool)));
    connect(&impl->fileManager, SIGNAL(error()),
            this, SLOT(handleFileError()));
    connect(impl->trackerClient.get(), SIGNAL(peerListUpdated(QList<TorrentPeer>)),
            this, SLOT(addToPeerList(QList<TorrentPeer>)));
    connect(impl->trackerClient.get(), SIGNAL(stopped()),
            this, SIGNAL(stopped()));
}

TorrentClient::~TorrentClient()
{
    qDebug() << "Destroy "
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";
    qDeleteAll(impl->peers);
    qDeleteAll(impl->pendingPieces);
    delete impl;
}

bool TorrentClient::setTorrent(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly) || !setTorrent(file.readAll())) {
        impl->setError(TorrentClientState::MetainfoParsingError);
        return false;
    }
    return true;
}

bool TorrentClient::setTorrent(const QByteArray &torrentData)
{
    qDebug() << "Setting torrent "
             << " TorrentClient";
    auto parser = MetainfoParser();
    QScopedPointer<Metainfo> metainfo(parser.parse(torrentData));

    if (metainfo.isNull()) {
        impl->setError(TorrentClientState::MetainfoParsingError);
        return false;
    }

    getState().getMetainfo() = *metainfo;

    QByteArray infoData = metainfo->getInfo().getInfoData();
    getState().setInfoHash(QCryptographicHash::hash(infoData, QCryptographicHash::Sha1));

    qint64 totalSize = 0;
    for (auto element : getState().getMetainfo().getInfo().getFileEntries()) {
        totalSize += element.fileSize;
    }
    getState().setTotalSize(totalSize);
    impl->setState(TorrentClientState::IdleState);
    impl->setError(TorrentClientState::NoError);

    qDebug() << "Set torrent " << metainfo->getInfo().getName()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    return true;
}

qint64 TorrentClient::getProgress() const
{
    return impl->lastProgressValue;
}

int TorrentClient::getConnectedPeerCount() const
{
    int cnt = 0;
    for (auto client : impl->connections) {
        if (client->getSocket().state() == QAbstractSocket::ConnectedState)
            ++cnt;
    }
    return cnt;
}

int TorrentClient::getSeedCount() const
{
    int cnt = 0;
    for (auto client : impl->connections) {
        if (client->getState().getPeerPieces().count(true) == state.getPieceCount())
            ++cnt;
    }
    return cnt;
}

TorrentClientState &TorrentClient::getState()
{
    return state;
}

void TorrentClient::start()
{
    if (getState().getState() != TorrentClientState::IdleState)
        return;

    qDebug() << "Starting torrent client " << getState().getMetainfo().getInfo().getName()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    impl->setState(TorrentClientState::InitializationState);

    QSharedPointer<FileStorage> storage(new FileStorage());
    storage->setFolder(state.getStorageFolder());
    storage->setPieceLength(state.getMetainfo().getInfo().getPieceLength());

    QList<FileEntry> entries;
    for (auto entry : state.getMetainfo().getInfo().getFileEntries()) {
        entries << FileEntry(qint64(entry.fileSize), entry.md5Hash, entry.relativePath);
    }
    storage->setEntries(entries);

    if (!storage->createStorage()) {
        impl->setError(TorrentClientState::FilesystemError);
        qDebug() << "Cannot create storage " << getState().getMetainfo().getInfo().getName()
                 << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";
        return;
    }

    impl->fileManager.setStorage(storage);
    impl->fileManager.setPieceHashes(state.getMetainfo().getInfo().getPieceHashes());
    impl->fileManager.setVerifiedPieces(impl->completedPieces);
    impl->fileManager.start(QThread::LowestPriority);
    impl->fileManager.requestFullVerification();
    qDebug() << "Started " << getState().getMetainfo().getInfo().getName()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";
}

void TorrentClient::stop()
{
    if (state.getState() == TorrentClientState::StoppingState)
        return;

    qDebug() << "Stopping " << getState().getMetainfo().getInfo().getName()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    TorrentClientState::TorrentClientWorkingState previousState = state.getState();
    impl->setState(TorrentClientState::StoppingState);

    if (impl->statisticsUpdateTimer != -1) {
        killTimer(impl->statisticsUpdateTimer);
        impl->statisticsUpdateTimer = -1;
    }

    emit downloadRateUpdated(0);
    emit uploadRateUpdated(0);

    for (auto client : impl->connections) {
        SendReceiveController::getInstance().removePeer(client);
        TorrentConnectionManager::getInstance()->removeClient(client);
        client->getSocket().abort();
    }
    impl->connections.clear();

    qDebug() << "Stopping completing client"
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    if (impl->completedPieces.count(true) == state.getPieceCount()) {
        impl->setState(TorrentClientState::CompleteState);
        emit completed();
    } else if (previousState > TorrentClientState::InitializationState) {
        impl->trackerClient->stop();
    } else {
        impl->setState(TorrentClientState::IdleState);
        emit stopped();
    }
}

void TorrentClient::setPaused(bool paused)
{
    qDebug() << "Pausing " << getState().getMetainfo().getInfo().getName()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";
    if (getState().getState() == TorrentClientState::CompleteState)
        return;

    impl->isPaused = paused;

    if (paused) {
        impl->setState(TorrentClientState::PausedState);
        for (auto client : impl->connections)
            client->getSocket().disconnectFromHost();
        impl->connections.clear();
    } else {
        impl->setState(TorrentClientState::SearchingState);
        connectToPeers();
    }
}

void TorrentClient::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == impl->uploadScheduleTimer) {
        scheduleUploads();
        return;
    }

    if (event->timerId() != impl->statisticsUpdateTimer) {
        QObject::timerEvent(event);
        return;
    }

    qint64 averageUpload = 0;
    qint64 averageDownload = 0;
    for (auto element : impl->uploadRateSamples) {
        averageUpload += element;
    }
    for (auto element : impl->downloadRateSamples) {
        averageDownload += element;
    }
    averageUpload /= qint64(impl->uploadRateSamples.size());
    averageDownload /= qint64(impl->downloadRateSamples.size());

    impl->uploadRateSamples.enqueue(0);
    impl->downloadRateSamples.enqueue(0);

    if (impl->uploadRateSamples.size() > RATE_CONTROL_WINDOW_LENGTH)
        impl->uploadRateSamples.dequeue();
    if (impl->downloadRateSamples.size() > RATE_CONTROL_WINDOW_LENGTH)
        impl->downloadRateSamples.dequeue();

    emit uploadRateUpdated(int(averageUpload));
    emit downloadRateUpdated(int(averageDownload));

    if (averageUpload == 0 && averageDownload == 0) {
        killTimer(impl->statisticsUpdateTimer);
        impl->statisticsUpdateTimer = -1;
    }
}


void TorrentClient::sendBlockToPeer(int requestId, const Block &block)
{
    qDebug() << "Sending block(" << block.getIdentifier().getPieceIndex()
             << ", " << block.getIdentifier().getOffset()
             << ", " << block.getIdentifier().getLength() << ")"
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    PWPClient *client = impl->readRequests.value(requestId);
    if (client) {
        if ((client->getState().getSelf() & PWPConnectionState::Choked) == 0)
            client->sendPiece(block);
    }
    impl->readRequests.remove(requestId);
}


void TorrentClient::fullVerificationDone(int requestId)
{
    impl->completedPieces = impl->fileManager.getVerifiedPieces();
    impl->incompletePieces.resize(impl->completedPieces.size());
    state.setPieceCount(impl->completedPieces.size());

    qDebug() << "Full verification done "
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    for (int i = 0; i < state.getPieceCount(); ++i) {
        if (!impl->completedPieces.testBit(i))
            impl->incompletePieces.setBit(i);
    }

    updateProgress(0);

    for (auto it = impl->pendingPieces.begin();
         it != impl->pendingPieces.end();) {
        if (impl->completedPieces.testBit(it.key()))
            it = impl->pendingPieces.erase(it);
        else
            ++it;
    }

    impl->uploadScheduleTimer = startTimer(UPLOAD_SCHEDULING_INTERVAL);

    if (impl->completedPieces.count(true) == state.getPieceCount()) {
        impl->setState(TorrentClientState::CompleteState);
        impl->trackerClient->setComplete(true);
     } else
        impl->setState(TorrentClientState::SearchingState);

    qDebug() << "Starting tracker"
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    impl->trackerClient->setAnnounceUrl(state.getMetainfo().getAnnounceTracker());
    impl->trackerClient->setTorrentClientState(&state);
    impl->trackerClient->setTorrentGlobalState(&TorrentGlobalState::getInstance());
    impl->trackerClient->start();

    if (state.getState() == TorrentClientState::CompleteState)
        impl->completeTorrent();
}


void TorrentClient::pieceVerified(int requestId, qint64 pieceIndex, bool ok)
{
    qDebug() << "Piece verified " << pieceIndex << " " << (ok ? "OK" : "NOT_OK")
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    TorrentPiece *piece = impl->pendingPieces.value(pieceIndex);

    for (auto it = impl->payloads.begin();
         it != impl->payloads.end();) {
        if (it.value()->pieceIndex == pieceIndex)
            it = impl->payloads.erase(it);
        else
            ++it;
    }

    if (!ok) {
        piece->isInProgress = false;
        piece->completedBlocks.fill(false);
        piece->requestedBlocks.fill(false);
        impl->callScheduler();
        return;
    }

    for (auto peer : impl->peers) {
        if (!peer->getInteresting())
            continue;
        bool interesting = false;
        for (int i = 0; i < state.getPieceCount(); ++i) {
            if (peer->getPiecesHave().testBit(i) && impl->incompletePieces.testBit(i)) {
                interesting = true;
                break;
            }
        }
        peer->setInteresting(interesting);
    }
    delete piece;


    impl->pendingPieces.remove(pieceIndex);
    impl->completedPieces.setBit(pieceIndex);
    impl->incompletePieces.clearBit(pieceIndex);

    for (auto client : impl->connections) {
        if (client->getSocket().state() == QAbstractSocket::ConnectedState
            && !client->getState().getPeerPieces().testBit(pieceIndex)) {
            client->sendHave(pieceIndex);
        }
    }

    int completedPiecesCount = impl->completedPieces.count(true);
    updateProgress(0);

    if (completedPiecesCount == state.getPieceCount()) {
        impl->completeTorrent();
    } else {
        if (completedPiecesCount == 1)
            impl->setState(TorrentClientState::DownloadingState);
        impl->callScheduler();
    }
}


void TorrentClient::handleFileError()
{
    qDebug() << "File error "
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    if (state.getState() == TorrentClientState::PausedState)
        return;
    setPaused(true);
    emit errorFired(TorrentClientState::FilesystemError);
}

void TorrentClient::connectToPeers()
{
    qDebug() << "Connecting to peers"
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    impl->isConnectingToClients = false;

    if (state.getState() == TorrentClientState::StoppingState ||
            state.getState() == TorrentClientState::IdleState ||
            state.getState() == TorrentClientState::PausedState ||
            state.getState() == TorrentClientState::CompleteState)
        return;

    if (state.getState() == TorrentClientState::SearchingState)
        impl->setState(TorrentClientState::ConnectingState);

    QList<TorrentPeer *> chosingPeers = impl->peers;

    while (!chosingPeers.isEmpty() &&
           TorrentConnectionManager::getInstance()->canAddClient() &&
           impl->connections.size() < MAX_CONNECTIONS_PER_CLIENT) {
        PWPClient *client = new PWPClient(this);

        client->getState().setSelfPeerId(TorrentGlobalState::getInstance().getClientId());
        SendReceiveController::getInstance().addPeer(client);
        TorrentConnectionManager::getInstance()->addClient(client);

        initializeConnection(client);
        impl->connections << client;

        int peerIndex = QRandomGenerator::global()->bounded(chosingPeers.size());
        TorrentPeer *peer = chosingPeers.takeAt(peerIndex);
        chosingPeers.removeAll(peer);

        peer->setConnectStart(QDateTime::currentSecsSinceEpoch());
        peer->setLastVisited(peer->getConnectStart());

        client->getState().setTorrentPeer(peer);
        client->connectToHost(peer->getAddress(), peer->getPort());
    }

    qDebug() << "Connected to peers"
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";
}

void TorrentClient::setupIncomingConnection(PWPClient *client)
{
    qDebug() << "Setting up incoming " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << getState().getMetainfo().getInfo().getName() << "";

    initializeConnection(client);

    SendReceiveController::getInstance().addPeer(client);
    impl->connections << client;

    client->getState().setInfoHash(state.getInfoHash());
    client->initialize(state.getPieceCount());
    client->sendBitfield(impl->completedPieces);

    emit peerInfoUpdated();

    if (impl->connections.isEmpty())
        scheduleUploads();
}

void TorrentClient::setupOutgoingConnection()
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());

    qDebug() << "Peer connected " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName();

    for (auto peer : impl->peers) {
        if (peer->getPort() == client->getSocket().peerPort() &&
                peer->getAddress() == client->getSocket().peerAddress()) {
            peer->setConnectTime(peer->getLastVisited() - peer->getConnectTime());
            break;
        }
    }

    client->getState().setInfoHash(state.getInfoHash());
    client->initialize(state.getPieceCount());
    client->sendBitfield(impl->completedPieces);

    emit peerInfoUpdated();
}

void TorrentClient::initializeConnection(PWPClient *client)
{
    connect(client, SIGNAL(connectionTimeout()),
            this, SLOT(removeClient()));

    connect(client, SIGNAL(pendingRequestTimeout()),
            this, SLOT(removeClient()));

    connect(client, SIGNAL(connected()),
            this, SLOT(setupOutgoingConnection()));

    connect(client, SIGNAL(disconnected()),
            this, SLOT(removeClient()));

    connect(client, SIGNAL(socketError(QAbstractSocket::SocketError)),
            this, SLOT(removeClient()));

    connect(client, SIGNAL(bitfieldReceived(QBitArray)),
            this, SLOT(peerUpdateAvailable(QBitArray)));

    connect(client, SIGNAL(requestReceived(BlockIdentifier)),
            this, SLOT(peerRequestsBlock(BlockIdentifier)));

    connect(client, SIGNAL(pieceReceived(Block)),
            this, SLOT(blockReceived(Block)));

    connect(client, SIGNAL(chokedReceived()),
            this, SLOT(peerChoked()));

    connect(client, SIGNAL(unchokedReceived()),
            this, SLOT(peerUnchoked()));

    connect(client, SIGNAL(bytesSent(qint64)),
            this, SLOT(pwpBytesWritten(qint64)));

    connect(client, SIGNAL(bytesReceived(qint64)),
            this, SLOT(pwpBytesReceived(qint64)));
}


void TorrentClient::removeClient()
{
    PWPClient *client = static_cast<PWPClient *>(sender());

    qDebug() << "Removing peer " << client->getSocket().peerAddress() << " : "  << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName();

    if (client->getState().getTorrentPeer() &&
            client->getSocket().state() == QAbstractSocket::ConnectedState)
        impl->peers.removeAll(client->getState().getTorrentPeer());

    SendReceiveController::getInstance().removePeer(client);
    impl->connections.removeAll(client);

    for (auto it = impl->payloads.find(client);
         it != impl->payloads.end() && it.key() == client;) {
        TorrentPiece *piece = it.value();
        piece->isInProgress = false;
        piece->requestedBlocks.fill(false);
        it = impl->payloads.erase(it);
    }

    QMapIterator<int, PWPClient *> it(impl->readRequests);
    while (it.findNext(client))
            impl->readRequests.remove(it.key());

    QObject::disconnect(client, SIGNAL(disconnected()),
                        this, SLOT(removeClient()));
    client->deleteLater();

    TorrentConnectionManager::getInstance()->removeClient(client);

    qDebug() << "Removed peer"
             << " TorrentClient " << state.getMetainfo().getInfo().getName();
    emit peerInfoUpdated();
    impl->callPeerConnector();
}

void TorrentClient::peerUpdateAvailable(const QBitArray &pieces)
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());

    qDebug() << "Peer availability update " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName();

    TorrentPeer *peer = nullptr;
    for (auto it = impl->peers.begin();
         it != impl->peers.end();
         it++) {
        if ((*it)->getAddress() == client->getSocket().peerAddress() &&
                (*it)->getPort() == client->getSocket().peerPort()) {
            peer = *it;
            break;
        }
    }

    if (pieces.count(true) == state.getPieceCount()) {
        if (peer)
            peer->setSeed(true);

        emit peerInfoUpdated();

        if (state.getState() == TorrentClientState::CompleteState) {
            client->getSocket().abort();
            return;
        } else {
            if (peer)
                peer->setInteresting(true);

            if ((client->getState().getSelf() & PWPConnectionState::Interested) == 0)
                client->sendInterested();

            qDebug() << "Scheduling seed " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                     << " TorrentClient " << state.getMetainfo().getInfo().getName();

            impl->callScheduler();
            return;
        }
    }

    if (peer) {
        peer->setPiecesHave(pieces);
        peer->setNumCompletedPieces(pieces.count(true));
    }

    bool interested = false;
    for (int pieceIndex = 0; pieceIndex < state.getPieceCount(); pieceIndex++) {
        if (!pieces.testBit(pieceIndex))
            continue;
        if (!impl->completedPieces.testBit(pieceIndex)) {
            interested = true;
            if ((client->getState().getSelf() & PWPConnectionState::Interested) == 0) {
                if (peer)
                    peer->setInteresting(true);
                client->sendInterested();
            }

            int inProgressCount = 0;

            for (auto it = impl->payloads.find(client);
                 it != impl->payloads.end() && it.key() == client;
                 it++) {
                if (it.value()->isInProgress)
                    inProgressCount += it.value()->requestedBlocks.count(true);
            }

            if (inProgressCount == 0) {

                qDebug() << "No blocks in progress for " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                         <<  " calling scheduler"
                         << " TorrentClient " << state.getMetainfo().getInfo().getName();

                impl->callScheduler();
            }
            break;
        }
    }

    if (!interested && (client->getState().getOther() & PWPConnectionState::Interested)) {
        if (peer)
            peer->setInteresting(false);
        client->sendNotInterested();
    }
}

void TorrentClient::peerRequestsBlock(const BlockIdentifier &identifier)
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());

    if (client->getState().getOther() & PWPConnectionState::Choked)
        return;

    if (!impl->completedPieces.testBit(identifier.getPieceIndex()))
        return;

    impl->readRequests.insert(impl->fileManager.requestRead(identifier), client);
}

void TorrentClient::blockReceived(const Block &block)
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());

    if (block.getData().size() == 0) {
        client->getSocket().abort();
        return;
    }

    int blockIndex = block.getOffset() / BLOCK_SIZE;
    TorrentPiece *piece = impl->pendingPieces.value(block.getPieceIndex());

    if (!piece || piece->completedBlocks.testBit(blockIndex)) {
        requestMore(client);
        return;
    }

    if (state.getState() != TorrentClientState::DownloadingState &&
            impl->completedPieces.count(true) > 0)
        impl->setState(TorrentClientState::DownloadingState);

    impl->fileManager.requestWrite(block);
    piece->completedBlocks.setBit(blockIndex);
    piece->requestedBlocks.clearBit(blockIndex);

    if (blocksLeftForPiece(piece) == 0) {
        impl->fileManager.requestVerification(piece->pieceIndex);

        for (auto it = impl->payloads.begin();
             it != impl->payloads.end();) {
            if (!it.value() || it.value()->pieceIndex == piece->pieceIndex)
                it = impl->payloads.erase(it);
            else
                it++;
        }
    }

    requestMore(client);
}

void TorrentClient::pwpBytesWritten(qint64 size)
{
    if (impl->statisticsUpdateTimer == -1)
        impl->statisticsUpdateTimer = startTimer(STATISTICS_UPDATE_INTEVAL);

    impl->uploadRateSamples.first() += size;
    impl->uploadedBytes += size;

    emit dataSent(size);
}

void TorrentClient::pwpBytesReceived(qint64 size)
{
    if (impl->statisticsUpdateTimer == -1)
        impl->statisticsUpdateTimer = startTimer(STATISTICS_UPDATE_INTEVAL);

    impl->downloadRateSamples.first() += size;
    impl->downloadedBytes += size;

    emit dataReceived(size);
}

int TorrentClient::blocksLeftForPiece(const TorrentPiece *piece) const
{
    int blocksLeftCount = 0;
    int completedBlocksSize = piece->completedBlocks.size();
    for (int i = 0; i < completedBlocksSize; ++i) {
        if (!piece->completedBlocks.testBit(i))
            ++blocksLeftCount;
    }
    return blocksLeftCount;
}

void TorrentClient::scheduleUploads()
{
    QList<PWPClient *> clients = impl->connections;
    QMultiMap<int, PWPClient *> speedClients;
    for (auto client : clients) {
        if (client->getSocket().state() == QAbstractSocket::ConnectedState &&
                client->getState().getPeerPieces().count(true) != state.getPieceCount()) {
            speedClients.insert(client->getDownloadSpeed(), client);
        }
    }

    int maxSpeedClients = MAX_SPEED_COUNT;
    QMapIterator<int, PWPClient *> it(speedClients);
    it.toBack();
    while (it.hasPrevious()) {
        PWPClient *client = it.previous().value();
        bool isInterested = (client->getState().getOther() & PWPConnectionState::Interested);

        if (maxSpeedClients > 0) {
            clients.removeAll(client);
            if (client->getState().getOther() & PWPConnectionState::Choked)
                client->sendUnchoke();
            --maxSpeedClients;
            continue;
        }

        if ((client->getState().getOther() & PWPConnectionState::Choked) == 0) {
            client->sendChoke();
            clients.removeAll(client);
        }
        if (!isInterested)
            clients.removeAll(client);
    }
}

void TorrentClient::scheduleDownloads()
{
    impl->isSchedulerCalled = false;

    if (state.getState() == TorrentClientState::StoppingState ||
            state.getState() == TorrentClientState::PausedState ||
            state.getState() == TorrentClientState::IdleState ||
            state.getState() == TorrentClientState::CompleteState)
        return;

    for (auto client : impl->connections)
        schedulePieceForClient(client);
}

void TorrentClient::schedulePieceForClient(PWPClient *client)
{
    if (client->getSocket().state() != QTcpSocket::ConnectedState)
        return;

    if (client->getState().getSelf() & PWPConnectionState::Choked)
        return;

    qDebug() << "Scheduling piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    QList<int> requestedPieces;
    bool isSomePiecesAreNotInProgress = false;
    TorrentPiece *lastPiece = 0;

    for (auto it = impl->payloads.find(client);
         it != impl->payloads.end() && it.key() == client;
         it++) {
        lastPiece = it.value();
        if (lastPiece->isInProgress) {
            requestedPieces << lastPiece->pieceIndex;
        } else {
            isSomePiecesAreNotInProgress = true;
        }
    }

    if (client->getState().getIncomingBlockRequests().size() >= MAX_PENDING_BLOCK_COUNT)
        return;

    if (!isSomePiecesAreNotInProgress || client->getState().getIncomingBlockRequests().size() > 0)
        lastPiece = 0;

    if (!lastPiece) {
        qDebug() << "Scheduling [choosing] piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                 << " TorrentClient " << state.getMetainfo().getInfo().getName();

        QBitArray incompleteAvailablePieces = impl->incompletePieces;

        incompleteAvailablePieces &= client->getState().getPeerPieces();

        for (auto pieceIndex : requestedPieces)
            incompleteAvailablePieces.clearBit(pieceIndex);

        if (incompleteAvailablePieces.count(true) == 0)
            return;

        qDebug() << "Scheduling [choosing2] piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                 << " TorrentClient " << state.getMetainfo().getInfo().getName();

        TorrentPiece * partialPiece = nullptr;
        for (auto it = impl->pendingPieces.begin();
             it != impl->pendingPieces.end();
             it++) {
            TorrentPiece *piece = it.value();
            if (incompleteAvailablePieces.testBit(it.key()) && !piece->isInProgress) {
                partialPiece = piece;
                break;
            }
        }
        if (partialPiece)
            lastPiece = partialPiece;


        qDebug() << "Scheduling [choosing2.5] piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                 << " TorrentClient " << state.getMetainfo().getInfo().getName();

        if (!lastPiece) {
            qDebug() << "Scheduling [choosing3] piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                     << " TorrentClient " << state.getMetainfo().getInfo().getName();

            int pieceIndex = 0;

            QList<int> incompleteIndeces;
            for (int i = 0; i < incompleteAvailablePieces.size(); ++i) {
                if (incompleteAvailablePieces.testBit(i))
                    incompleteIndeces << i;
            }
            pieceIndex = incompleteIndeces.first();

            lastPiece = new TorrentPiece();
            lastPiece->pieceIndex = pieceIndex;
            lastPiece->length = impl->fileManager.getStorage()->getPieceLengthAt(pieceIndex);
            int numBlocks = lastPiece->length / BLOCK_SIZE;
            if (lastPiece->length % BLOCK_SIZE)
                ++numBlocks;
            lastPiece->completedBlocks.resize(numBlocks);
            lastPiece->requestedBlocks.resize(numBlocks);
            impl->pendingPieces.insert(pieceIndex, lastPiece);

            qDebug() << "Scheduled piece " << pieceIndex
                     << " for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
                     << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";
        }

        lastPiece->isInProgress = true;
        impl->payloads.insert(client, lastPiece);
    }
    qDebug() << "Scheduled piece for client " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName();


    requestMore(client);
}


void TorrentClient::requestMore(PWPClient *client)
{
    int blocksInProgressCount = client->getState().getIncomingBlockRequests().size();
    QList<TorrentPiece *> piecesInProgress;

    for (auto it = impl->payloads.find(client);
         it != impl->payloads.end() && it.key() == client;
         it++) {
        if (it.value()->isInProgress)
            piecesInProgress << it.value();
    }

    if (piecesInProgress.isEmpty() && impl->incompletePieces.count(true)) {
        impl->callScheduler();
        return;
    }

    int maxInProgress = MAX_PENDING_BLOCK_COUNT;
    if (blocksInProgressCount >= maxInProgress)
        return;

    for (auto piece : piecesInProgress) {
        blocksInProgressCount += requestBlocks(client, piece, maxInProgress - blocksInProgressCount);
        if (blocksInProgressCount >= maxInProgress)
            break;
    }

    if (blocksInProgressCount < maxInProgress)
        impl->callScheduler();
}

int TorrentClient::requestBlocks(PWPClient *client, TorrentPiece *piece, int maxBlocks)
{
    QVector<int> incompleteBlockIndeces;
    int blocksCount = piece->completedBlocks.size();
    for (int i = 0; i < blocksCount; ++i) {
        if (!piece->completedBlocks.testBit(i) && !piece->requestedBlocks.testBit(i))
            incompleteBlockIndeces << i;
    }

    if (incompleteBlockIndeces.size() == 0)
        return 0;

    qDebug() << "Requesting blocks " << maxBlocks
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    int blocksToRequest = qMin(maxBlocks, incompleteBlockIndeces.size());

    for (int i = 0; i < blocksToRequest; ++i) {

        int blockSize = BLOCK_SIZE;
        if ((piece->length % BLOCK_SIZE > 0) && incompleteBlockIndeces.at(i) == blocksCount - 1)
            blockSize = piece->length % BLOCK_SIZE;

        client->sendRequest(BlockIdentifier(piece->pieceIndex,
                                            incompleteBlockIndeces.at(i) * BLOCK_SIZE,
                                            blockSize));
        piece->requestedBlocks.setBit(incompleteBlockIndeces.at(i));
    }

    return blocksToRequest;
}

void TorrentClient::peerChoked()
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());
    if (!client)
        return;

    qDebug() << "We are choked by " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    for (auto it = impl->payloads.find(client);
         it != impl->payloads.end() && it.key() == client;) {
        it.value()->isInProgress = false;
        it.value()->requestedBlocks.fill(false);
        it = impl->payloads.erase(it);
    }
}

void TorrentClient::peerUnchoked()
{
    PWPClient *client = qobject_cast<PWPClient *>(sender());
    if (!client)
        return;

    qDebug() << "We are choked by " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    impl->callScheduler();
}

void TorrentClient::addToPeerList(const QList<TorrentPeer> &peerList)
{
    qDebug() << "New peers " << peerList.size()
             << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

    foreach (TorrentPeer peer, peerList) {
        bool isKnown = false;
        for (auto knownPeer : impl->peers) {
            if (knownPeer->getPort() == peer.getPort()
                && knownPeer->getAddress() == peer.getAddress()) {
                isKnown = true;
                break;
            }
        }
        if (!isKnown) {
            TorrentPeer *newPeer = new TorrentPeer();
            newPeer->setPort(peer.getPort());
            newPeer->setAddress(peer.getAddress());
            newPeer->setPeerId(peer.getPeerId());
            newPeer->setInteresting(true);
            newPeer->setSeed(false);
            newPeer->setLastVisited(0);
            newPeer->setConnectStart(0);
            newPeer->setConnectTime(1e9);
            newPeer->setPiecesHave(QBitArray(state.getPieceCount()));
            newPeer->setNumCompletedPieces(0);
            impl->peers << newPeer;
            qDebug() << "New peer " << peer.getAddress() << " : " << peer.getPort()
                     << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";

        }
    }

    int maxPeers = TorrentGlobalState::getInstance().getMaxConnections() * 3;
    if (impl->peers.size() > maxPeers) {
        QSet<TorrentPeer *> alivePeers;
        for (auto peer : impl->peers) {
            for (auto client : impl->connections) {
                if (client->getState().getTorrentPeer() == peer &&
                        (client->getDownloadSpeed() + client->getUploadSpeed()) > ALIVE_SPEED_LIMIT)
                    alivePeers << peer;
            }
        }

        for (auto it = impl->peers.begin();
             it != impl->peers.end();) {
            if (!alivePeers.contains(*it)) {
                it = impl->peers.erase(it);
            } else {
                it++;
            }
        }

        while (impl->peers.size() > maxPeers)
            impl->peers.takeFirst();
    }

    if (state.getState() == TorrentClientState::SearchingState)
        connectToPeers();
    else if (state.getState() == TorrentClientState::InitializationState ||
             state.getState() == TorrentClientState::ConnectingState)
        impl->callPeerConnector();
}

void TorrentClient::trackerStopped()
{
    if (state.getState() != TorrentClientState::CompleteState)
        impl->setState(TorrentClientState::IdleState);
    emit stopped();
}

void TorrentClient::updateProgress(int requestId, int progress)
{
    if (progress == -1 && state.getPieceCount() > 0) {
        int newProgress = (impl->completedPieces.count(true) * 100) / state.getPieceCount();
        qDebug() << "Updating progress " << newProgress
                 << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";
        if (impl->lastProgressValue != newProgress) {
            impl->lastProgressValue = newProgress;
            emit progressUpdated(newProgress);
        }
    } else if (impl->lastProgressValue != progress) {
        impl->lastProgressValue = progress;
        qDebug() << "Updating progress " << progress
                 << " TorrentClient " << state.getMetainfo().getInfo().getName() << "";
        emit progressUpdated(progress);
    }
}

TorrentGlobalState *TorrentClient::getGlobalState() const
{
    return globalState;
}

void TorrentClient::setGlobalState(TorrentGlobalState *value)
{
    globalState = value;
}

bool TorrentClient::getPaused() const
{
    return impl->isPaused;
}
