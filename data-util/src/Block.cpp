#include "include/Block.h"

Block::Block(qint64 pieceIndex, qint64 offset, const QByteArray &data)
    : pieceIndex(pieceIndex), offset(offset), data(data)
{

}

qint64 Block::getPieceIndex() const
{
    return pieceIndex;
}

qint64 Block::getOffset() const
{
    return offset;
}

const QByteArray &Block::getData() const
{
    return data;
}

BlockIdentifier Block::getIdentifier() const
{
    return BlockIdentifier(pieceIndex, offset, data.length());
}
