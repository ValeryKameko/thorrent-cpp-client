#ifndef ADDTORRENTDIALOG_H
#define ADDTORRENTDIALOG_H

#include <QDialog>

#include <metainfo/Metainfo.h>

namespace Ui {
class AddTorrentDialog;
}

class AddTorrentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddTorrentDialog(QWidget *parent = nullptr);
    ~AddTorrentDialog();

    QString getDestinationFolder() const;
    void setDestinationFolder(const QString &value);

    QString getTorrentFilePath() const;
    void setTorrentFilePath(const QString &value);

    void clearTorrentInfo();
    void updateTorrentInfo(const QString &filePath);
    void updateTorrentInfo(const metainfo::Metainfo &metainfo);

    void updateDestinationFolder(const QString &destination);
    void updateOkButton();

    metainfo::Metainfo getMetainfo() const;
    void setMetainfo(const metainfo::Metainfo &value);

private slots:
    void on_openTorrentFileButton_clicked();

    void on_openDestinationButton_clicked();

    void on_torrentFileValueEdit_textChanged(const QString &filePath);

private:

    Ui::AddTorrentDialog *ui;

    bool isValidMetainfo;
    bool isValidDestination;
    QString destinationFolder;
    QString torrentFilePath;
    metainfo::Metainfo metainfo;
};

#endif // ADDTORRENTDIALOG_H
