#include "torrenttableview.h"
#include "ui_torrenttableview.h"

#include "uicommon.h"
#include <QFileInfo>
#include <QMessageBox>
#include <TorrentClient.h>

TorrentTableView::TorrentTableView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TorrentTableView)
{
    ui->setupUi(this);
}

TorrentTableView::~TorrentTableView()
{
    delete ui;
}

void TorrentTableView::addTorrent(const QString &filePath, const QString &destinationFolder)
{
    TorrentClient *client = new TorrentClient(this);
    if (!client->setTorrent(filePath)) {
        QMessageBox messageBox(
                    QMessageBox::Icon::Warning,
                    "Error",
                    "Torrent file cannot be opened",
                    QMessageBox::StandardButton::Ok);
        return;
    }

    client->setTorrent(filePath);
    client->getState().setStorageFolder(destinationFolder);

    QObject::connect(client, SIGNAL(stateChanged(TorrentClientState::TorrentClientWorkingState)),
                     this, SLOT(torrentStateChanged(TorrentClientState::TorrentClientWorkingState)));
    QObject::connect(client, SIGNAL(progressUpdated(qint64)),
                     this, SLOT(torrentProgressUpdated(qint64)));
    TorrentRow *row = new TorrentRow();
    row->client = client;
    row->name = QFileInfo(filePath).fileName();

    torrents << row;

    int rowIndex = ui->torrentsTableWidget->rowCount();
    ui->torrentsTableWidget->setRowCount(rowIndex + 1);

    updateRow(rowIndex);
    ui->torrentsTableWidget->setItem(rowIndex, 2,
                                     new QTableWidgetItem(UICommon::formatSize(0) + "/s"));
    ui->torrentsTableWidget->setItem(rowIndex, 3,
                                     new QTableWidgetItem(UICommon::formatSize(0) + "/s"));


    connect(client, SIGNAL(stateChanged(TorrentClientState::TorrentClientWorkingState)),
            this, SLOT(torrentStateChanged(TorrentClientState::TorrentClientWorkingState)));

    connect(client, SIGNAL(uploadRateUpdated(qint64)),
            this, SLOT(torrentUploadSpeedUpdated(qint64)));

    connect(client, SIGNAL(downloadRateUpdated(qint64)),
            this, SLOT(torrentDownloadSpeedUpdated(qint64)));

    client->start();
}

void TorrentTableView::deleteTorrent()
{
    QItemSelectionModel *selectionModel = this->ui->torrentsTableWidget->selectionModel();
    if (!selectionModel->hasSelection())
        return;

    QModelIndexList rows = selectionModel->selectedRows();

    QList<TorrentClient *> removingClients;
    for (QModelIndex element : rows) {
        int row = element.row();
        torrents[row]->client->stop();
        removingClients << torrents[row]->client;
    }

    for (auto client : removingClients) {
        int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
        torrents.removeAt(rowIndex);
        ui->torrentsTableWidget->removeRow(rowIndex);
        delete client;
    }
}

void TorrentTableView::pauseTorrent()
{
    QItemSelectionModel *selectionModel = this->ui->torrentsTableWidget->selectionModel();
    if (!selectionModel->hasSelection())
        return;

    QModelIndexList rows = selectionModel->selectedRows();

    for (QModelIndex element : rows) {
        int row = element.row();
        bool isPaused = torrents[row]->client->getPaused();
        torrents[row]->client->setPaused(!isPaused);
    }
}

void TorrentTableView::torrentStateChanged(TorrentClientState::TorrentClientWorkingState state)
{
    TorrentClient *client = qobject_cast<TorrentClient *>(sender());
    int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
    updateRow(rowIndex);
}

void TorrentTableView::torrentProgressUpdated(qint64 progress)
{
    TorrentClient *client = qobject_cast<TorrentClient *>(sender());
    int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
    updateRow(rowIndex);
}

void TorrentTableView::torrentDownloadSpeedUpdated(qint64 bytesPerSecond)
{
    TorrentClient *client = qobject_cast<TorrentClient *>(sender());
    if (client->getState().getState() != TorrentClientState::DownloadingState)
        return;
    int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
    ui->torrentsTableWidget->setItem(rowIndex, 2,
                                     new QTableWidgetItem(UICommon::formatSize(bytesPerSecond) + "/s"));
}

void TorrentTableView::torrentUploadSpeedUpdated(qint64 bytesPerSecond)
{
    TorrentClient *client = qobject_cast<TorrentClient *>(sender());
    if (client->getState().getState() != TorrentClientState::DownloadingState)
        return;
    int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
    ui->torrentsTableWidget->setItem(rowIndex, 3,
                                     new QTableWidgetItem(UICommon::formatSize(bytesPerSecond) + "/s"));
}

void TorrentTableView::torrentStopped()
{
    TorrentClient *client = qobject_cast<TorrentClient *>(sender());
    int rowIndex = getTorrentRowIndex(getTorrentRowByClient(client));
    ui->torrentsTableWidget->setItem(rowIndex, 2,
                                     new QTableWidgetItem(UICommon::formatSize(0) + "/s"));
    ui->torrentsTableWidget->setItem(rowIndex, 4,
                                     new QTableWidgetItem(UICommon::formatSize(0) + "/s"));
}

void TorrentTableView::updateRow(int rowIndex)
{
    QString name = torrents.at(rowIndex)->name;
    TorrentClient *client = torrents.at(rowIndex)->client;

    ui->torrentsTableWidget->setItem(rowIndex, 0,
                                     new QTableWidgetItem(name));
    ui->torrentsTableWidget->setItem(rowIndex, 1,
                                     new QTableWidgetItem(client->getState().getStateString()));
    int progress = client->getProgress();
    if (progress <= 0)
        progress = 0;

    ui->torrentsTableWidget->setItem(rowIndex, 4,
                                     new QTableWidgetItem(QString("%1%").arg(progress)));
}

TorrentRow *TorrentTableView::getTorrentRowByClient(TorrentClient *client)
{
    for (auto torrentRow : torrents) {
        if (torrentRow->client == client)
            return torrentRow;
    }
    return nullptr;
}

int TorrentTableView::getTorrentRowIndex(TorrentRow *row)
{
    for (int i = 0; i < torrents.length(); i++) {
        if (torrents[i] == row)
            return i;
    }
    return -1;
}
