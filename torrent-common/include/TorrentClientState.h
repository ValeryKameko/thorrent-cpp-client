#ifndef TORRENTCLIENTSTATE_H
#define TORRENTCLIENTSTATE_H

#include <QByteArray>
#include <QString>

#include <metainfo/Metainfo.h>


class TorrentClientState
{
public:
    enum TorrentClientWorkingState {
        IdleState,
        PausedState,
        StoppingState,
        InitializationState,
        SearchingState,
        ConnectingState,
        DownloadingState,
        CompleteState
    };

    enum TorrentClientError {
        NoError,
        UnknownError,
        TorrentServerError,
        MetainfoParsingError,
        TrackerError,
        FilesystemError
    };

    TorrentClientState();

    QByteArray getInfoHash() const;
    void setInfoHash(const QByteArray &value);

    qint64 getDownloadedSize() const;
    void setDownloadedSize(const qint64 &value);

    qint64 getUploadedSize() const;
    void setUploadedSize(const qint64 &value);

    TorrentClientWorkingState getState() const;
    void setState(const TorrentClientWorkingState &value);

    qint64 getTotalSize() const;
    void setTotalSize(const qint64 &value);

    QString getErrorString() const;
    void setErrorString(const QString &value);

    TorrentClientError getError() const;
    void setError(const TorrentClientError &value);

    metainfo::Metainfo &getMetainfo();

    QString getStorageFolder() const;
    void setStorageFolder(const QString &value);

    QString getStateString() const;
    void setStateString(const QString &value);

    int getPieceCount() const;
    void setPieceCount(int value);

private:
    QByteArray infoHash;

    qint64 downloadedSize;
    qint64 uploadedSize;
    qint64 totalSize;

    QString errorString;
    metainfo::Metainfo metainfo;

    QString stateString;

    QString storageFolder;

    int pieceCount;

    TorrentClientError error;
    TorrentClientWorkingState state;
};


#endif // TORRENTCLIENTSTATE_H
