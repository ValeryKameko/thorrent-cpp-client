#-------------------------------------------------
#
# Project created by QtCreator 2019-06-09T05:51:54
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = torrent-client
TEMPLATE = lib

DEFINES += TORRENT_CLIENT_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += include

SOURCES += \
    src/TorrentClient.cpp

HEADERS += \
    include/TorrentClient.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../bencoding/release/ -lbencoding
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../bencoding/debug/ -lbencoding
else:unix: LIBS += -L$$OUT_PWD/../bencoding/ -lbencoding

INCLUDEPATH += $$PWD/../bencoding/include
DEPENDPATH += $$PWD/../bencoding/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../metainfo/release/ -lmetainfo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../metainfo/debug/ -lmetainfo
else:unix: LIBS += -L$$OUT_PWD/../metainfo/ -lmetainfo

INCLUDEPATH += $$PWD/../metainfo/include
DEPENDPATH += $$PWD/../metainfo/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../torrent-common/release/ -ltorrent-common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../torrent-common/debug/ -ltorrent-common
else:unix: LIBS += -L$$OUT_PWD/../torrent-common/ -ltorrent-common

INCLUDEPATH += $$PWD/../torrent-common/include
DEPENDPATH += $$PWD/../torrent-common/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../tracker-client/release/ -ltracker-client
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../tracker-client/debug/ -ltracker-client
else:unix: LIBS += -L$$OUT_PWD/../tracker-client/ -ltracker-client

INCLUDEPATH += $$PWD/../tracker-client/include
DEPENDPATH += $$PWD/../tracker-client/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../data-util/release/ -ldata-util
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../data-util/debug/ -ldata-util
else:unix: LIBS += -L$$OUT_PWD/../data-util/ -ldata-util

INCLUDEPATH += $$PWD/../data-util/include
DEPENDPATH += $$PWD/../data-util/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../send-receive-controller/release/ -lsend-receive-controller
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../send-receive-controller/debug/ -lsend-receive-controller
else:unix: LIBS += -L$$OUT_PWD/../send-receive-controller/ -lsend-receive-controller

INCLUDEPATH += $$PWD/../send-receive-controller/include
DEPENDPATH += $$PWD/../send-receive-controller/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/release/ -ltorrent-connection-manager
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/debug/ -ltorrent-connection-manager
else:unix: LIBS += -L$$OUT_PWD/../torrent-connection-manager/ -ltorrent-connection-manager

INCLUDEPATH += $$PWD/../torrent-connection-manager/include
DEPENDPATH += $$PWD/../torrent-connection-manager/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pwp-client/release/ -lpwp-client
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pwp-client/debug/ -lpwp-client
else:unix: LIBS += -L$$OUT_PWD/../pwp-client/ -lpwp-client

INCLUDEPATH += $$PWD/../pwp-client/include
DEPENDPATH += $$PWD/../pwp-client/include
