#ifndef SENDRECEIVECONTROLLER_H
#define SENDRECEIVECONTROLLER_H

#include <QSet>
#include <QDateTime>
#include <QObject>

class PWPClient;


class SendReceiveController : public QObject
{
    Q_OBJECT
public:
    static SendReceiveController &getInstance();

    void addPeer(PWPClient *client);
    void removePeer(PWPClient *client);

    qint64 getUploadRate() const;
    void setUploadRate(const qint64 &value);

    qint64 getDownloadRate() const;
    void setDownloadRate(const qint64 &value);

private slots:
    void scheduleSendReceive();
    void processSendReceive();

private:
    SendReceiveController();

    QSet<PWPClient *> peers;
    QTime time;

    qint64 uploadRate;
    qint64 downloadRate;

    bool isSendReceiveScheduled;
    int sendReceiveTimer;
};

#endif // SENDRECEIVECONTROLLER_H
