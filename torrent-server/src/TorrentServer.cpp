#include "include/TorrentServer.h"

TorrentServer::TorrentServer(QObject *parent)
    : QObject(parent), server()
{

}

void TorrentServer::setSendReceiveController(const QSharedPointer<SendReceiveController> &controller)
{
    this->controller = controller;
}

void TorrentServer::setConnectionManager(const QSharedPointer<TorrentConnectionManager> &manager)
{

    this->manager = manager;
}

void TorrentServer::addClient(PWPClient *client)
{
    clients << client;
}

void TorrentServer::removeClient(PWPClient *client)
{
    clients.removeAll(client);
}

QTcpServer &TorrentServer::getServerSocket()
{
    return server;
}

const QTcpServer &TorrentServer::getServerSocket() const
{
    return server
}
