#include "uicommon.h"

#include <QList>

static const QList<QString> SIZE_FORMAT_SUFFIXES = QList<QString> {
        "KB", "MB", "GB", "TB"
};

static const int ONE_KB = 1024;

UICommon::UICommon()
{

}

QString UICommon::formatSize(qint64 size)
{
    if (size < ONE_KB)
        return QString::asprintf("%d bytes", size);
    double relativeSize = size;
    for (auto formatSuffix : SIZE_FORMAT_SUFFIXES) {
        relativeSize /= 1024;
        if (relativeSize < ONE_KB)
            return QString::asprintf("%.2f %s", relativeSize, formatSuffix.constData());
    }
    return QString("inf");
}
