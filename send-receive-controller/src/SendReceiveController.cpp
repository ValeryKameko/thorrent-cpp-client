#include "include/SendReceiveController.h"
#include <QTcpSocket>
#include <QTimer>
#include <PWPClient.h>
#include <QHostAddress>

static constexpr int READ_BUFFER_SIZE_COEFFICENT = 3;
static constexpr int SCHEDULE_SEND_RECEIVE_DELAY = 50;
static constexpr int MAX_SCHEDULE_INTERVAL = 1000;

SendReceiveController::SendReceiveController()
{
    isSendReceiveScheduled = false;
    scheduleSendReceive();
}

SendReceiveController &SendReceiveController::getInstance()
{
    static SendReceiveController instance;
    return instance;
}

void SendReceiveController::addPeer(PWPClient *client)
{
    qDebug() << "Add peer " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " to SendReceiveController";
    peers.insert(client);

    QObject::connect(client, SIGNAL(readyToTransfer()),
                     this, SLOT(scheduleSendReceive()));

    client->getSocket().setReadBufferSize(READ_BUFFER_SIZE_COEFFICENT * downloadRate);
    scheduleSendReceive();
}

void SendReceiveController::removePeer(PWPClient *client)
{
    qDebug() << "Remove peer " << client->getSocket().peerAddress() << " : " << client->getSocket().peerPort()
             << " to SendReceiveController";

    QObject::disconnect(client, SIGNAL(readyToTransfer()),
                     this, SLOT(scheduleSendReceive()));

    client->getSocket().setReadBufferSize(0);
    peers.remove(client);
}

qint64 SendReceiveController::getUploadRate() const
{
    return uploadRate;
}

void SendReceiveController::setUploadRate(const qint64 &value)
{
    qDebug() << "New upload speed " << value << " of"
             << " SendReceiveController";

    uploadRate = value;
}

qint64 SendReceiveController::getDownloadRate() const
{
    return downloadRate;
}

void SendReceiveController::setDownloadRate(const qint64 &value)
{
    qDebug() << "New download speed " << value << " of"
             << " SendReceiveController";

    downloadRate = value;

    for (auto entry : peers) {
        entry->getSocket().setReadBufferSize(READ_BUFFER_SIZE_COEFFICENT * downloadRate);
    }
    scheduleSendReceive();
}

void SendReceiveController::scheduleSendReceive()
{
    if (isSendReceiveScheduled)
        return;
    isSendReceiveScheduled = true;
    QTimer::singleShot(SCHEDULE_SEND_RECEIVE_DELAY, this, SLOT(processSendReceive()));
}

void SendReceiveController::processSendReceive()
{
    if (!isSendReceiveScheduled)
        return;
    isSendReceiveScheduled = false;

    int interval = MAX_SCHEDULE_INTERVAL;
    if (!time.isNull())
        interval = qMin<int>(interval, time.elapsed());

    qint64 needReadSize = (downloadRate * interval) / 1000;
    qint64 needWriteSize = (uploadRate * interval) / 1000;
    if (needReadSize == 0 && needWriteSize == 0) {
        scheduleSendReceive();
        return;
    }

    QSet<PWPClient *> processingClients;
    for (auto entry : peers) {
        if (entry->getSocket().state() != QAbstractSocket::ConnectedState) {
            continue;
        }
        processingClients.insert(entry);
    }

    time.start();
    bool canReadWriteMore = false;

    while ((needReadSize > 0 || needWriteSize > 0) && !processingClients.isEmpty()) {
        canReadWriteMore = false;
        qint64 needReadChunk = needReadSize / processingClients.size();
        if (needReadChunk == 0)
            needReadChunk = 1;
        qint64 needWriteChunk = needWriteSize / processingClients.size();
        if (needWriteChunk == 0)
            needWriteChunk = 1;

        for (QSetIterator<PWPClient *> it(processingClients);
             it.hasNext() && (needReadChunk > 0 || needWriteChunk > 0);) {
            needReadChunk = qMin(needReadSize, needReadChunk);
            needWriteChunk = qMin(needWriteSize, needWriteChunk);

            PWPClient *client = it.next();
            bool wasReadWrite = false;

            if (client->getSocket().state() != QAbstractSocket::ConnectedState) {
                processingClients.remove(client);
                continue;
            }

            qint64 readAvailable = qMin(client->getSocketBytesAvailable(), needReadChunk);
            if (readAvailable > 0) {
                qint64 readCount = client->readFromSocket(readAvailable);
                if (readCount > 0) {
                    needReadSize -= readCount;
                    wasReadWrite = true;
                }
            }

            qint64 writeAvailable = qMin(client->getBytesToWrite(), needWriteChunk);
            if (writeAvailable > 0) {
                qint64 writeCount = client->writeToSocket(writeAvailable);
                if (writeCount > 0) {
                    needWriteSize -= writeCount;
                    wasReadWrite = true;
                }
            }

            if (wasReadWrite && client->canTransfer())
                canReadWriteMore = true;
            else
                processingClients.remove(client);
        }
    };

    if (canReadWriteMore || needReadSize == 0 || needWriteSize == 0) {
        scheduleSendReceive();
    }
}
