#-------------------------------------------------
#
# Project created by QtCreator 2019-06-08T09:32:00
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = torrent-common
TEMPLATE = lib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    $$files(src/*.cpp, true)

HEADERS += \
    $$files(include/*.h, true)

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../metainfo/release/ -lmetainfo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../metainfo/debug/ -lmetainfo
else:unix: LIBS += -L$$OUT_PWD/../metainfo/ -lmetainfo

INCLUDEPATH += $$PWD/../metainfo/include
DEPENDPATH += $$PWD/../metainfo/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../bencoding/release/ -lbencoding
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../bencoding/debug/ -lbencoding
else:unix: LIBS += -L$$OUT_PWD/../bencoding/ -lbencoding

INCLUDEPATH += $$PWD/../bencoding/include
DEPENDPATH += $$PWD/../bencoding/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/release/ -ltorrent-connection-manager
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/debug/ -ltorrent-connection-manager
else:unix: LIBS += -L$$OUT_PWD/../torrent-connection-manager/ -ltorrent-connection-manager

INCLUDEPATH += $$PWD/../torrent-connection-manager/include
DEPENDPATH += $$PWD/../torrent-connection-manager/include
