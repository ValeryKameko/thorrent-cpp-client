#ifndef PIECE_H
#define PIECE_H

#include <QByteArray>



class Piece
{
public:
    Piece(qint64 pieceIndex, const QByteArray &data);

    qint64 getPieceIndex() const;
    const QByteArray &getData() const;

private:
    qint64 pieceIndex;
    QByteArray data;
};

#endif // PIECE_H
