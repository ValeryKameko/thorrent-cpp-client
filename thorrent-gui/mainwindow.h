#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "torrenttableview.h"

#include <QLabel>
#include <QMainWindow>
#include <QSlider>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionAdd_Torrent_triggered();

    void downloadSliderValueChanged(int value);
    void uploadSliderValueChanged(int value);

    void on_actionStop_Torrent_triggered();

    void on_actionPause_Torrent_triggered();

private:
    void addTorrent(const QString &filePath, const QString &destinationFolder);
    void createSliders();

    Ui::MainWindow *ui;

    QSlider *downloadSpeedSlider;
    QSlider *uploadSpeedSlider;

    QLabel *downloadSpeedLabel;
    QLabel *uploadSpeedLabel;

    TorrentTableView *torrentTableView;
};

#endif // MAINWINDOW_H
