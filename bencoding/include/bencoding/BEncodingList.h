#ifndef BENCODINGLIST_H
#define BENCODINGLIST_H

#include "BEncodingObject.h"

#include <QList>

namespace bencoding {

class BEncodingList : public BEncodingObject
{
public:
    BEncodingList();
    BEncodingList(const BEncodingList &other);
    BEncodingList(BEncodingObject *element);
    BEncodingList(const QList<BEncodingObject *> &list);

    BEncodingObject * deepCopy() const override;

    virtual ~BEncodingList() override;

    virtual QByteArray encodeToByteArray() const override;

    const QList<BEncodingObject*> &getList() const;
    QList<BEncodingObject*> &getList();
private:
    QList<BEncodingObject*> list;
};

}

#endif // BENCODINGLIST_H
