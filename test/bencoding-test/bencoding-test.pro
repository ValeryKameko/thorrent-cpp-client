QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    tst_bencodingtest.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../bencoding/release/ -lbencoding
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../bencoding/debug/ -lbencoding
else:unix: LIBS += -L$$OUT_PWD/../../bencoding/ -lbencoding

INCLUDEPATH += $$PWD/../../bencoding/include
DEPENDPATH += $$PWD/../../bencoding/include
