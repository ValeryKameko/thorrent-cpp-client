#include "include/PWPConnectionState.h"

PWPConnectionState::PWPConnectionState(QObject *parent)
    : QObject(parent),
      incomingBlockRequests(), pendingBlocks(), pendingBlocksSize(0),
      self(Choked), other(Choked),
      recevedHandshake(false), sentHandshake(false),
      gotPeerId(false), infoHash(), selfPeerId(), otherPeerId(),
      peerPieces()
{

}

QQueue<BlockIdentifier> &PWPConnectionState::getIncomingBlockRequests()
{
    return incomingBlockRequests;
}

const QQueue<BlockInfo> &PWPConnectionState::getPendingBlocks() const
{
    return pendingBlocks;
}

QQueue<BlockInfo> &PWPConnectionState::getPendingBlocks()
{
    return pendingBlocks;
}

qint64 PWPConnectionState::getPendingBlocksSize()
{
    return pendingBlocksSize;
}

PWPConnectionState::PWPState PWPConnectionState::getSelf() const
{
    return self;
}

PWPConnectionState::PWPState PWPConnectionState::getOther() const
{
    return other;
}

void PWPConnectionState::setSelf(const PWPState &value)
{
    self = value;
}

void PWPConnectionState::setOther(const PWPState &value)
{
    other = value;
}

bool PWPConnectionState::getRecevedHandshake() const
{
    return recevedHandshake;
}

void PWPConnectionState::setRecevedHandshake(bool value)
{
    recevedHandshake = value;
}

bool PWPConnectionState::getSentHandshake() const
{
    return sentHandshake;
}

void PWPConnectionState::setSentHandshake(bool value)
{
    sentHandshake = value;
}

bool PWPConnectionState::getGotPeerId() const
{
    return gotPeerId;
}

void PWPConnectionState::setGotPeerId(bool value)
{
    gotPeerId = value;
}

QByteArray PWPConnectionState::getSelfPeerId() const
{
    return selfPeerId;
}

void PWPConnectionState::setSelfPeerId(const QByteArray &value)
{
    selfPeerId = value;
}

TorrentPeer *PWPConnectionState::getTorrentPeer() const
{
    return torrentPeer;
}

void PWPConnectionState::setTorrentPeer(TorrentPeer *value)
{
    torrentPeer = value;
}


QByteArray PWPConnectionState::getInfoHash() const
{
    return infoHash;
}

void PWPConnectionState::setInfoHash(const QByteArray &value)
{
    infoHash = value;
}

QBitArray &PWPConnectionState::getPeerPieces()
{
    return peerPieces;
}

QByteArray PWPConnectionState::getOtherPeerId() const
{
    return otherPeerId;
}

void PWPConnectionState::setOtherPeerId(const QByteArray &value)
{
    otherPeerId = value;
}

void PWPConnectionState::setPendingBlocksSize(const qint64 &value)
{
    pendingBlocksSize = value;
}
