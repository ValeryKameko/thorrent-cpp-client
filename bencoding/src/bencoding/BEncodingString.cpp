#include "bencoding/BEncodingString.h"

#include <QString>

namespace bencoding {

BEncodingString::BEncodingString(const QByteArray &value)
    : BEncodingObject(), value(value)
{

}

BEncodingString::BEncodingString(const QString &value)
    : BEncodingObject(), value(value.toLatin1())
{

}

BEncodingString::BEncodingString()
    : BEncodingObject(), value()
{

}

BEncodingObject *BEncodingString::deepCopy() const
{
    return new BEncodingString(this->value);
}

BEncodingString::BEncodingString(const BEncodingString &other)
    : BEncodingObject(), value(other.value)
{

}

BEncodingString::~BEncodingString()
{

}

QByteArray BEncodingString::encodeToByteArray() const
{
    QByteArray result;
    result += QString::number(value.length());
    result += ":";
    result += value;
    return result;
}

const QByteArray &BEncodingString::getValue() const
{
    return value;
}

QString BEncodingString::getStringValue() const
{
    return QString::fromLatin1(value);
}

void BEncodingString::setValue(const QByteArray &value)
{
    this->value = value;
}

void BEncodingString::setValue(const QString &value)
{
    this->value = value.toLatin1();
}

bool BEncodingString::operator<(const BEncodingString &other) const
{
    return value < other.value;
}

bool BEncodingString::operator==(const BEncodingString &other) const
{
    return value == other.value;
}

}

uint qHash(const bencoding::BEncodingString &obj)
{
    return qHash(obj.getValue());
}
