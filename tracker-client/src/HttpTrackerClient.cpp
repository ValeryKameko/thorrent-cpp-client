#include "include/HttpTrackerClient.h"

#include <bencoding/BEncodingParser.h>

#include <QAuthenticator>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>
#include <QTimerEvent>
#include <QUrlQuery>
#include <QtEndian>
#include <TorrentGlobalState.h>


static constexpr int INITIAL_REQUEST_INTERVAL = 300;
static constexpr int PEERS_COUNT_WANT = 10;

using namespace bencoding;

HttpTrackerClient::HttpTrackerClient(QObject *parent)
    : TrackerClient(parent)
{
    firstRequest = true;
    isCompleted = false;
    lastRequest = false;
    isWorking = false;
    requestTimerId = -1;
    requestTimerInterval = INITIAL_REQUEST_INTERVAL;

    QObject::connect(&httpClient, &QNetworkAccessManager::finished,
                     this, &HttpTrackerClient::handleResponse);
}

HttpTrackerClient::~HttpTrackerClient()
{
    stop();
}

void HttpTrackerClient::setAnnounceUrl(const QUrl &url)
{
    this->url = url;
}

void HttpTrackerClient::setComplete(bool isCompleted)
{
    this->isCompleted = isCompleted;
}

void HttpTrackerClient::start()
{
    qDebug() << "Starting tracker " << url << "";
    if (!isWorking)
        announce();
    emit started();
}

void HttpTrackerClient::stop()
{
    qDebug() << "Stopping tracker " << url << "";
    lastRequest = true;
    if (!isWorking)
        announce();
}

void HttpTrackerClient::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == requestTimerId) {
        announce();
    } else {
        TrackerClient::timerEvent(event);
    }
}

void HttpTrackerClient::announce()
{
    QUrl queryUrl(url);
    QUrlQuery query(queryUrl);

    if (!trackerId.isEmpty())
        query.addQueryItem("trackerid", trackerId);
    query.addQueryItem("info_hash", QString::fromLatin1(percentEncode(clientState->getInfoHash())));
    query.addQueryItem("peer_id", QUrl::toPercentEncoding(globalState->getClientId()));
    query.addQueryItem("port", QByteArray::number(globalState->getListeningPort()));
    query.addQueryItem("compact", QByteArray::number(1));
    query.addQueryItem("uploaded", QByteArray::number(clientState->getUploadedSize()));
    query.addQueryItem("downloaded", QByteArray::number(clientState->getDownloadedSize()));
    query.addQueryItem("numwant", QByteArray::number(PEERS_COUNT_WANT));
    query.addQueryItem("left", QByteArray::number(
                           qMax<qint64>(0, clientState->getTotalSize() - clientState->getDownloadedSize())));

    if (isCompleted) {
        query.addQueryItem("event", "completed");
    } else if (firstRequest) {
        query.addQueryItem("event", "started");
    } else if (lastRequest) {
        query.addQueryItem("event", "stopped");
    }

    queryUrl.setQuery(query.toString(QUrl::ComponentFormattingOption::FullyEncoded));

    qDebug() << "query url " << queryUrl.url();

    if (!queryUrl.userName().isEmpty()) {
        userName = queryUrl.userName();
        userPassword = queryUrl.password();

        QObject::connect(&httpClient, SIGNAL(authenticationRequired(QNetworkReply*, QAutentificator*)),
                this, SLOT(provideAuthentication(QNetworkReply*, QAutentificator*)));
    }

    QNetworkRequest request(queryUrl);
    request.setHeader(QNetworkRequest::KnownHeaders::UserAgentHeader, QString("Thorrent 1.0"));
    qDebug() << "Send tracker request " << request.url().toString(QUrl::ComponentFormattingOption::FullyEncoded) << "";
    httpClient.get(request);
}

void HttpTrackerClient::provideAuthentification(QNetworkReply *reply, QAuthenticator *authenticator)
{
    authenticator->setUser(userName);
    authenticator->setPassword(userPassword);
}

void HttpTrackerClient::handleResponse(QNetworkReply *reply)
{
    QString key;
    reply->deleteLater();
    if (lastRequest) {
        isWorking = false;
        emit stopped();
        return;
    }

    if (reply->error() != QNetworkReply::NoError) {
        errorString = QString("Connection error: %1").arg(reply->errorString());
        isWorking = false;
        emit error();
        emit stopped();
        return;
    }

    QByteArray response = reply->readAll();
    reply->abort();

    BEncodingParser parser;
    QScopedPointer<BEncodingDictionary> dictionary(parser.parseDictionary(response));

    if (!dictionary) {
        warningString = QString("Reply parsing error");
        qDebug() << "Tracker warning" << warningString << "";
        isWorking = false;
        emit warning();
        emit stopped();
        return;
    }

    key = QString::fromUtf8("warning message");
    if (dictionary->getMap().contains(key)) {
        warningString = QString("Warning from tracker: %1").arg(key);
        qDebug() << "Tracker warning" << warningString << "";
        isWorking = false;
        emit warning();
        emit stopped();
        return;
    }

    if (dictionary->getMap().contains(QString::fromUtf8("failure reason"))) {
        errorString = QString("Failure: %1").arg(dictionary->getMap().value(QString::fromUtf8("failure reason"))->encodeToString());
        qDebug() << "Tracker error " << errorString << "";
        isWorking = false;
        emit error();
        emit stopped();
        return;
    }

    key = QString::fromUtf8("tracker id");
    if (dictionary->getMap().contains(key))
        trackerId = dictionary->getMap().value(key)->encodeToByteArray();

    key = QString::fromUtf8("interval");
    if (dictionary->getMap().contains(key)) {
        auto interval = dynamic_cast<BEncodingNumber *>(dictionary->getMap().value(key));
        if (interval) {
            if (requestTimerId != -1)
                killTimer(requestTimerId);
            requestTimerInterval = interval->getIntValue() * 1000;
            isWorking = true;
            requestTimerId = startTimer(requestTimerInterval);
        }
        qDebug() << "Tracker update interval " << requestTimerInterval << "";
    }

    key = QString::fromUtf8("peers");
    if (dictionary->getMap().contains(key)) {
        peers.clear();

        auto peersList = dynamic_cast<BEncodingList *>(dictionary->getMap().value(key));
        auto peersString = dynamic_cast<BEncodingString *>(dictionary->getMap().value(key));

        if (peersString) {
            QByteArray peersData = peersString->getValue();
            for (int i = 5; i < peersData.size(); i += 6) {
                TorrentPeer peer;
                auto peerEntryStart = peersData.constData() + i - 5;
                quint32 ip = qFromBigEndian<quint32>(peerEntryStart);
                quint16 port = qFromBigEndian<quint16>(peerEntryStart + 4);

                peer.setPort(port);
                peer.setAddress(QHostAddress(ip));

                peers << peer;
            }
        } else {
            for (auto entry : peersList->getList()) {
                auto entryDictionary = dynamic_cast<BEncodingDictionary *>(entry);
                if (!entryDictionary)
                    continue;

                TorrentPeer peer;
                BEncodingNumber *number;
                BEncodingString *string;
                quint32 ip;
                quint16 port;
                QByteArray peerId;

                key = QString::fromUtf8("ip");
                if (!entryDictionary->getMap().contains(key) ||
                        !(number = dynamic_cast<BEncodingNumber *>(entryDictionary->getMap().value(key))))
                    continue;
                ip = static_cast<quint32>(number->getInt64Value());

                key = QString::fromUtf8("port");
                if (!entryDictionary->getMap().contains(key) ||
                        !(number = dynamic_cast<BEncodingNumber *>(entryDictionary->getMap().value(key))))
                    continue;
                port = static_cast<quint16>(number->getInt16Value());

                key = QString::fromUtf8("peer id");
                if (!entryDictionary->getMap().contains(key) ||
                        !(string = dynamic_cast<BEncodingString *>(entryDictionary->getMap().value(key))))
                    continue;
                peerId = string->getValue();

                peer.setPort(port);
                peer.setAddress(QHostAddress(ip));
                peer.setPeerId(peerId);

                peers << peer;
            }
        }
        qDebug() << "Peers received from tracker " << peers.length() << "";

        emit peerListUpdated(peers);
    }
}

QByteArray HttpTrackerClient::percentEncode(const QByteArray &data)
{
    QByteArray result;
    for (int i = 0; i < data.length(); i++) {
        result += '%';
        result += QByteArray::number(quint8(data[i]), 16).rightJustified(2, '0', true);
    }
    return result;
}
