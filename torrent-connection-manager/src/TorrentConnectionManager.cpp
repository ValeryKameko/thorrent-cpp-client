#include "include/TorrentConnectionManager.h"

#include <QRandomGenerator>
#include <QDatastream>
#include <QDebug>


static constexpr int MAX_CLIENT_COUNT = 20;
static const QByteArray TORRENT_PEER_ID_PREFIX = QByteArray("-TH1000-");
static constexpr int PEER_ID_SIZE = 20;

TorrentConnectionManager *TorrentConnectionManager::getInstance()
{
    static TorrentConnectionManager *instance = nullptr;
    if (!instance)
        instance = new TorrentConnectionManager();
    return instance;
}

bool TorrentConnectionManager::canAddClient()
{
    return (peers.size() < MAX_CLIENT_COUNT);
}

int TorrentConnectionManager::getClientLimit() const
{
    return MAX_CLIENT_COUNT;
}

void TorrentConnectionManager::addClient(PWPClient *client)
{
    peers.insert(client);
}

void TorrentConnectionManager::removeClient(PWPClient *client)
{
    peers.remove(client);
}

QByteArray TorrentConnectionManager::generatePeerId()
{
    if (!peerId.isEmpty())
        return peerId;
    QByteArray buffer;
    buffer += TORRENT_PEER_ID_PREFIX;
    QRandomGenerator gen;
    while (buffer.size() < PEER_ID_SIZE) {
        qint8 digit = gen.bounded(10) + '0';
        buffer += digit;
    }

    peerId = buffer;

    qDebug() << "Our id = " << peerId
                << " TorrentConnectionManager";
    return peerId;
}

TorrentConnectionManager::TorrentConnectionManager()
{

}
