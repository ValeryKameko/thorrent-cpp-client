#ifndef BUFFEREDSOCKET_H
#define BUFFEREDSOCKET_H

#include "PWPConnectionState.h"

#include <QQueue>
#include <QTcpSocket>



class PWPClient : public QObject
{
    Q_OBJECT
public:
    PWPClient(QObject *parent = nullptr);

    void initialize(int piecesCount);

    const QTcpSocket &getSocket() const;
    QTcpSocket &getSocket();

    const PWPConnectionState &getState() const;
    PWPConnectionState &getState();

    void sendChoke();
    void sendUnchoke();
    void sendInterested();
    void sendKeepAlive();
    void sendNotInterested();
    void sendHave(int piece);
    void sendBitfield(const QBitArray &bitfield);
    void sendRequest(const BlockIdentifier &identifier);
    void sendCancel(const BlockIdentifier &identifier);
    void sendPiece(const Block &block);

    qint64 writeToSocket(qint64 count);
    qint64 readFromSocket(qint64 count);
    qint64 getDownloadSpeed() const;
    qint64 getUploadSpeed() const;

    bool canTransfer() const;
    qint64 getSocketBytesAvailable() const;
    qint64 getBytesAvailable() const;
    qint64 getBytesToWrite() const;

    void connectToHost(const QHostAddress &address,
                       quint16 port,
                       QIODevice::OpenMode openMode = QIODevice::ReadWrite);
    void diconnectFromHost();

signals:
    void bytesReceived(qint64 count);
    void bytesSent(qint64 count);

    void infoHashReceived(const QByteArray &infoHash);
    void readyToTransfer();

    void chokedReceived();
    void unchokedReceived();
    void interestedReceived();
    void notInterestedReceived();

    void bitfieldReceived(const QBitArray &pieces);
    void requestReceived(const BlockIdentifier &identifier);
    void pieceReceived(const Block &block);

    void socketError(QAbstractSocket::SocketError error);
    void connectionTimeout();
    void pendingRequestTimeout();
    void disconnected();
    void connected();

private:
    void timerEvent(QTimerEvent *event) override;

    qint64 writeData(const char *data, qint64 size);
    qint64 readData(char *data, qint64 size);
    QByteArray read(qint64 size);

private slots:
    void sendHandshake();
    void processIncomingMessages();
    bool processMessage(const QByteArray &message);

private:
    enum MessageType {
        ChokeMessage = 0,
        UnchokeMessage = 1,
        InterestedMessage = 2,
        NotInterestedMessage = 3,
        HaveMessage = 4,
        BitfieldMessage = 5,
        RequestMessage = 6,
        PieceMessage = 7,
        CancelMessage = 8
    };


    QTcpSocket socket;

    QByteArray incomingBuffer;
    QByteArray outgoingBuffer;

    QQueue<qint64> downloadSizeSamples;
    QQueue<qint64> uploadSizeSamples;
    int updateStatisticsTimer;

    int timeoutTimer;
    int pendingRequestTimer;
    bool invalidateTimeout;
    int keepAliveTimer;

    int nextMessageLength;

    PWPConnectionState state;
};

#endif // BUFFEREDSOCKET_H
