#include "include/TorrentGlobalState.h"

#include <TorrentConnectionManager.h>

const int MAX_CONNECTIONS = 10;
const int FAKE_LISTENING_PORT = 6881;

TorrentGlobalState::TorrentGlobalState()
    : clientId(), listeningPort(-1)
{

}

TorrentGlobalState &TorrentGlobalState::getInstance()
{
    static TorrentGlobalState instance;
    return instance;
}

QByteArray TorrentGlobalState::getClientId()
{
    if (clientId.isEmpty())
        clientId = TorrentConnectionManager::getInstance()->generatePeerId();
    return clientId;
}

int TorrentGlobalState::getListeningPort() const
{
    return FAKE_LISTENING_PORT;
}

int TorrentGlobalState::getMaxConnections() const
{
    return MAX_CONNECTIONS;
}
