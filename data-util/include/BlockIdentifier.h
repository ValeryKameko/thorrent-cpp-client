#ifndef BLOCKIDENTIFIER_H
#define BLOCKIDENTIFIER_H

#include <QtGlobal>

class BlockIdentifier
{
public:
    BlockIdentifier();
    BlockIdentifier(qint64 pieceIndex, qint64 offset, qint64 length);
    BlockIdentifier(const BlockIdentifier& other);

    qint64 getPieceIndex() const;

    qint64 getOffset() const;

    qint64 getLength() const;

    bool operator==(const BlockIdentifier &other) const;
private:
    qint64 pieceIndex;
    qint64 offset;
    qint64 length;
};

#endif // BLOCKIDENTIFIER_H
