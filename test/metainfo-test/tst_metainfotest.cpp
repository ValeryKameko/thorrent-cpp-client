#include <QtTest>

#include <metainfo/MetainfoParser.h>

using namespace metainfo;

class MetainfoTest : public QObject
{
    Q_OBJECT

public:
    MetainfoTest();
    ~MetainfoTest();

private slots:
    void MetainfoParser_shouldParseSingleFileTorrent();
    void MetainfoParser_shouldParseMultiFileTorrent();
    void MetainfoParser_shouldParseTestFileTorrent();

};

MetainfoTest::MetainfoTest()
{

}

MetainfoTest::~MetainfoTest()
{

}

void MetainfoTest::MetainfoParser_shouldParseSingleFileTorrent()
{
    QFile testFile(QCoreApplication::applicationDirPath() + "/resources/single_info_file.torrent");
    QVERIFY(testFile.open(QIODevice::ReadOnly));
    QByteArray data = testFile.readAll();
    testFile.close();

    auto parser = MetainfoParser();
    QScopedPointer<Metainfo> metainfo(parser.parse(data));
    QVERIFY(!metainfo.isNull());
}

void MetainfoTest::MetainfoParser_shouldParseMultiFileTorrent()
{
    QFile testFile(QCoreApplication::applicationDirPath() + "/resources/multi_info_file.torrent");
    QVERIFY(testFile.open(QIODevice::ReadOnly));
    QByteArray data = testFile.readAll();
    testFile.close();

    auto parser = MetainfoParser();
    QScopedPointer<Metainfo> metainfo(parser.parse(data));
    QVERIFY(!metainfo.isNull());
}

void MetainfoTest::MetainfoParser_shouldParseTestFileTorrent()
{

    QFile testFile(QCoreApplication::applicationDirPath() + "/resources/test.torrent");
    QVERIFY(testFile.open(QIODevice::ReadOnly));
    QByteArray data = testFile.readAll();
    testFile.close();

    auto parser = MetainfoParser();
    QScopedPointer<Metainfo> metainfo(parser.parse(data));
    QVERIFY(!metainfo.isNull());
}


QTEST_MAIN(MetainfoTest)

#include "tst_metainfotest.moc"
