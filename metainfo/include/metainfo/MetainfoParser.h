#ifndef METAINFOPARSER_H
#define METAINFOPARSER_H

#include "Metainfo.h"

#include <bencoding/BEncodingDictionary.h>
#include <bencoding/BEncodingParser.h>

namespace metainfo {

class MetainfoParser
{
public:
    MetainfoParser();

    Metainfo *parse(const QByteArray &byteArray);

private:
    void clear();

    template<class T>
    T * getDictionaryValue(bencoding::BEncodingDictionary *dict, const QString &key);

    bool parseAnnounce(bencoding::BEncodingString *string);
    bool parseAnnounceList(bencoding::BEncodingList *list);
    bool parseComment(bencoding::BEncodingString *string);
    bool parseCreatedBy(bencoding::BEncodingString *string);
    bool parseCreatedDate(bencoding::BEncodingNumber *number);

    bool parseInfo(bencoding::BEncodingDictionary *dictionary);
    bool parseName(bencoding::BEncodingString *string);
    bool parsePath(bencoding::BEncodingList *list);
    bool parseFiles(bencoding::BEncodingList *list);
    bool parsePieces(bencoding::BEncodingString *string);
    bool parsePieceLength(bencoding::BEncodingNumber *number);
    bool parseLength(bencoding::BEncodingNumber *number);
    bool parseMd5Hash(bencoding::BEncodingString *string);

    MetainfoBuilder builder;
    MetainfoInfoBuilder infoBuilder;

    MetainfoInfoFileEntry fileEntry;

    bencoding::BEncodingParser bencodingParser;
    bencoding::BEncodingDictionary *dict;
    bencoding::BEncodingDictionary *infoDict;
};

}

#endif // METAINFOPARSER_H
