#include "include/FileStorageWorker.h"
#include "Block.h"
#include <QtCore>

FileStorageWorker::FileStorageWorker(QObject *parent)
    : QThread(parent), verifiedPieces(), needQuit(false), storage(nullptr)
{

}

FileStorageWorker::~FileStorageWorker()
{
    needQuit.store(true);
    condition.wakeOne();
    wait();
}

int FileStorageWorker::requestRead(const BlockIdentifier &identifier)
{
    DataRequest request;
    request.type = DataRequest::ReadRequest;
    request.readRequest = new BlockIdentifier(identifier);
    request.requestId = requestId.fetchAndAddOrdered(1);

    qDebug() << "Request read block (" << identifier.getPieceIndex()
             << ", " << identifier.getOffset()
             << ", " << identifier.getLength()
             << ") of storage " << storage->getFolder() << "";

    QMutexLocker locker(&lock);
    requests << request;

    condition.wakeOne();
    return request.requestId;
}

int FileStorageWorker::requestWrite(const Block &block)
{
    DataRequest request;
    request.type = DataRequest::WriteRequest;
    request.writeRequest = new Block(block);
    request.requestId = requestId.fetchAndAddOrdered(1);

    qDebug() << "Request write block (" << block.getIdentifier().getPieceIndex()
             << ", " << block.getIdentifier().getOffset()
             << ", " << block.getIdentifier().getLength()
             << ") of storage " << storage->getFolder() << "";

    QMutexLocker locker(&lock);
    requests << request;

    condition.wakeOne();
    return request.requestId;
}

int FileStorageWorker::requestVerification(qint64 pieceIndex)
{
    DataRequest request;
    request.type = DataRequest::VerifyRequest;
    request.verifyRequest = pieceIndex;
    request.requestId = requestId.fetchAndAddOrdered(1);

    qDebug() << "Request verification of piece(" << pieceIndex
             << ") of storage " << storage->getFolder() << "";;

    QMutexLocker locker(&lock);
    requests << request;

    condition.wakeOne();
    return request.requestId;
}

int FileStorageWorker::requestFullVerification()
{
    DataRequest request;
    request.type = DataRequest::FullVerification;
    request.requestId = requestId.fetchAndAddOrdered(1);

    qDebug() << "Request full verification of storage " << storage->getFolder() << "";


    QMutexLocker locker(&lock);
    requests << request;

    condition.wakeOne();
    return request.requestId;
}

QBitArray FileStorageWorker::getVerifiedPieces() const
{
    QMutexLocker locker(&lock);
    return verifiedPieces;
}

void FileStorageWorker::setVerifiedPieces(const QBitArray &value)
{
    QMutexLocker locker(&lock);
    verifiedPieces = value;
}

QString FileStorageWorker::getErrorString() const
{
    return errorString;
}

QSharedPointer<FileStorage> FileStorageWorker::getStorage() const
{
    QMutexLocker locker(&lock);
    return storage;
}

void FileStorageWorker::setStorage(const QSharedPointer<FileStorage> &value)
{
    QMutexLocker locker(&lock);
    storage = value;
}

void FileStorageWorker::run()
{
    do {
        {
            QMutexLocker locker(&lock);
            if (needQuit.load() == 0 && requests.size() == 0) {
                condition.wait(&lock);
                qDebug() << "Waking"
                         << "of storage " << storage->getFolder() << "";
            }
            if (needQuit.load() != 0)
                break;
        }
        qDebug() << "Processing request"
                 << "of storage " << storage->getFolder() << "";
        lock.lock();
        DataRequest request = requests.dequeue();
        lock.unlock();

        QScopedPointer<Block> block;
        QScopedPointer<BlockIdentifier> identifier;
        bool ok;

        switch (request.type) {
        case DataRequest::ReadRequest:
            identifier.reset(request.readRequest);
            block.reset(storage->readBlock(*identifier));
            if (block.isNull()) {
                errorString = "Read error";
                qDebug() << "Read ERROR block (" << request.readRequest->getPieceIndex()
                         << ", " << request.readRequest->getOffset()
                         << ", " << request.readRequest->getLength()
                         << ") of storage " << storage->getFolder() << "";
                emit error();
            } else {
                qDebug() << "Read block (" << request.readRequest->getPieceIndex()
                         << ", " << request.readRequest->getOffset()
                         << ", " << request.readRequest->getLength()
                         << ") of storage " << storage->getFolder() << "";
                emit dataRead(request.requestId, *block);
            }
            break;

        case DataRequest::WriteRequest:
            block.reset(request.writeRequest);
            ok = storage->writeBlock(*block);
            if (!ok) {
                errorString = "Write error";
                qDebug() << "Write ERROR block (" << request.writeRequest->getIdentifier().getPieceIndex()
                         << ", " << request.writeRequest->getIdentifier().getOffset()
                         << ", " << request.writeRequest->getIdentifier().getLength()
                         << ") of storage " << storage->getFolder() << "";
                emit error();
            } else {
                qDebug() << "Write block (" << request.writeRequest->getIdentifier().getPieceIndex()
                         << ", " << request.writeRequest->getIdentifier().getOffset()
                         << ", " << request.writeRequest->getIdentifier().getLength()
                         << ") of storage " << storage->getFolder() << "";

                emit dataWritten(request.requestId, block->getIdentifier());
            }
            break;

        case DataRequest::VerifyRequest:
            lock.lock();
            ok = verifyPiece(request.verifyRequest);
            qDebug() << "Piece verificatioon " << request.verifyRequest << " is " << (ok ? "Ok" : "NOT_OK")
                     << " of storage " << storage->getFolder() << "";
            emit pieceVerified(request.requestId, request.verifyRequest, ok);
            lock.unlock();
            qDebug() << "Piece verification unlocked"
                     << " of storage " << storage->getFolder() << "";

            break;

        case DataRequest::FullVerification:
            lock.lock();
            qDebug() << "Start full verification"
                     << " of storage " << storage->getFolder() << "";
            verifyFull(request.requestId);
            qDebug() << "End full verification"
                     << " of storage " << storage->getFolder() << "";
            lock.unlock();
            break;
        }

    } while (needQuit.load() == 0);

    qDebug() << "Stopping worker"
             << " of storage " << storage->getFolder() << "";

    lock.lock();
    QQueue<DataRequest> tempRequests = requests;
    requests.clear();
    lock.unlock();

    while (tempRequests.size())  {
        DataRequest request = tempRequests.dequeue();

        QScopedPointer<Block> block;
        QScopedPointer<BlockIdentifier> identifier;
        bool ok;

        switch (request.type) {
        case DataRequest::ReadRequest:
            identifier.reset(request.readRequest);
            break;

        case DataRequest::WriteRequest:
            block.reset(request.writeRequest);
            ok = storage->writeBlock(*block);
            if (!ok) {
                errorString = "Write error";
                qDebug() << "Write ERROR block (" << request.writeRequest->getIdentifier().getPieceIndex()
                         << ", " << request.writeRequest->getIdentifier().getOffset()
                         << ", " << request.writeRequest->getIdentifier().getLength()
                         << ") of storage " << storage->getFolder() << "";

                emit error();
            } else {
                qDebug() << "Write block (" << request.writeRequest->getIdentifier().getPieceIndex()
                         << ", " << request.writeRequest->getIdentifier().getOffset()
                         << ", " << request.writeRequest->getIdentifier().getLength()
                         << ") of storage " << storage->getFolder() << "";

                emit dataWritten(request.requestId, block->getIdentifier());
            }
            break;

        case DataRequest::VerifyRequest:
            break;

        case DataRequest::FullVerification:
            break;
        }
    }
}

bool FileStorageWorker::verifyPiece(qint64 pieceIndex)
{
    QScopedPointer<Block> block(storage->readBlock(
                BlockIdentifier(pieceIndex, 0, storage->getPieceLengthAt(pieceIndex))));
    QByteArray hash = QCryptographicHash::hash(block->getData(), QCryptographicHash::Sha1);

    qDebug() << "Verify piece " << pieceIndex
             << " " << hash.toHex()
             << " but need " << pieceHashes.at(pieceIndex).toHex()
             << " of storage " << storage->getFolder() << "";

    if (hash != pieceHashes.at(pieceIndex))
        return false;

    verifiedPieces.setBit(pieceIndex, true);
    return true;
}

void FileStorageWorker::verifyFull(int requestId)
{
    qint64 piecesCount = storage->getPiecesCount();
    verifiedPieces.resize(piecesCount);
    verifiedPieces.fill(false);

    int prevPercent = 0;
    for (int i = 0; i < piecesCount; i++) {

        bool ok = verifyPiece(i);

        int percent = ((i + 1) * 100) / piecesCount;
        if (percent != prevPercent)
            emit fullVerificationProgress(requestId, percent);
        prevPercent = percent;
    }
    emit fullVerificationProgress(requestId, 100);
    emit fullVerificationDone(requestId);
}

QList<QByteArray> FileStorageWorker::getPieceHashes() const
{
    return pieceHashes;
}

void FileStorageWorker::setPieceHashes(const QList<QByteArray> &value)
{
    pieceHashes = value;
}

