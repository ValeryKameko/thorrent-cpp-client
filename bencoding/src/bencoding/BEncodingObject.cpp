#include "bencoding/BEncodingObject.h"

#include <QString>

namespace bencoding {

BEncodingObject::BEncodingObject()
{

}

BEncodingObject::~BEncodingObject()
{

}

QString BEncodingObject::encodeToString() const
{
    return QString(encodeToByteArray());
}

}
