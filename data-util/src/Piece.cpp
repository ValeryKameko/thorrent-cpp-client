#include "include/Piece.h"

Piece::Piece(qint64 pieceIndex, const QByteArray &data)
    : pieceIndex(pieceIndex), data(data)
{

}

qint64 Piece::getPieceIndex() const
{
    return pieceIndex;
}

const QByteArray &Piece::getData() const
{
    return data;
}
