#ifndef BENCODINGSTRING_H
#define BENCODINGSTRING_H

#include "BEncodingObject.h"

namespace bencoding {

class BEncodingString : public BEncodingObject
{
public:
    BEncodingString(const QByteArray& value);
    BEncodingString(const QString& value);
    BEncodingString(const BEncodingString &other);
    BEncodingString();

    BEncodingObject * deepCopy() const override;

    virtual ~BEncodingString() override;

    virtual QByteArray encodeToByteArray() const override;

    const QByteArray& getValue() const;
    QString getStringValue() const;
    void setValue(const QByteArray& value);
    void setValue(const QString& value);

    bool operator<(const BEncodingString &other) const;

    bool operator==(const BEncodingString &other) const;
private:
    QByteArray value;
};

}

inline uint qHash(const bencoding::BEncodingString &obj);

#endif // BENCODINGSTRING_H
