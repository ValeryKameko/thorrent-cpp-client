#include "include/bencoding/BEncodingParser.h"
#include "bencoding/BEncodingParser.h"

namespace bencoding {

BEncodingParser::BEncodingParser()
{

}

BEncodingObject *BEncodingParser::parse(const QByteArray &byteArray)
{
    BEncodingObject * obj;
    if ((obj = parseString(byteArray)))
        return obj;
    if ((obj = parseNumber(byteArray)))
        return obj;
    if ((obj = parseList(byteArray)))
        return obj;
    if ((obj = parseDictionary(byteArray)))
        return obj;
    return nullptr;
}

BEncodingDictionary *BEncodingParser::parseDictionary(const QByteArray &byteArray)
{
    BEncodingDictionary * obj;
    this->byteArray = byteArray;

    pointer = 0;
    if ((obj = parseDictionary()))
        return obj;
    return nullptr;
}

BEncodingList *BEncodingParser::parseList(const QByteArray &byteArray)
{
    BEncodingList * obj;
    this->byteArray = byteArray;

    pointer = 0;
    if ((obj = parseList()))
        return obj;
    return nullptr;
}

BEncodingNumber *BEncodingParser::parseNumber(const QByteArray &byteArray)
{
    BEncodingNumber * obj;
    this->byteArray = byteArray;

    pointer = 0;
    if ((obj = parseNumber()))
        return obj;
    return nullptr;
}

BEncodingString *BEncodingParser::parseString(const QByteArray &byteArray)
{
    BEncodingString * obj;
    this->byteArray = byteArray;

    pointer = 0;
    if ((obj = parseString()))
        return obj;
    return nullptr;
}

BEncodingObject *BEncodingParser::parse()
{
    BEncodingObject * obj;

    int savePointer = pointer;

    if ((obj = parseString()))
        return obj;
    pointer = savePointer;
    if ((obj = parseNumber()))
        return obj;
    pointer = savePointer;
    if ((obj = parseList()))
        return obj;
    pointer = savePointer;
    if ((obj = parseDictionary()))
        return obj;
    pointer = savePointer;
    return nullptr;

}

BEncodingDictionary *BEncodingParser::parseDictionary()
{
    BEncodingDictionary *dict = new BEncodingDictionary();
    if (pointer >= byteArray.size() || byteArray[pointer++] != 'd') {
        delete dict;
        return nullptr;
    }

    while (pointer < byteArray.size() && byteArray[pointer] != 'e') {
        BEncodingString *key = parseString();
        if (key == nullptr) {
            delete dict;
            return nullptr;
        }

        BEncodingObject *value = parse();
        if (value == nullptr) {
            delete dict;
            delete key;
            return nullptr;
        }
        dict->getMap().insert(*key, value);
        delete key;
    }

    if (pointer >= byteArray.size() || byteArray[pointer++] != 'e') {
        delete dict;
        return nullptr;
    }

    return dict;
}

BEncodingList *BEncodingParser::parseList()
{
    BEncodingList *list = new BEncodingList();
    if (pointer >= byteArray.size() || byteArray[pointer++] != 'l') {
        delete list;
        return nullptr;
    }

    while (pointer < byteArray.size() && byteArray[pointer] != 'e') {
        BEncodingObject *value = parse();
        if (value == nullptr) {
            delete list;
            return nullptr;
        }
        list->getList() << value;
    }

    if (pointer >= byteArray.size() || byteArray[pointer++] != 'e') {
        delete list;
        return nullptr;
    }

    return list;
}

BEncodingNumber *BEncodingParser::parseNumber()
{
    if (pointer >= byteArray.size() || byteArray[pointer++] != 'i')
        return nullptr;

    QByteArray numberPart;
    while (pointer < byteArray.size() && byteArray[pointer] != 'e' && numberPart.length() < 20) {
        numberPart += byteArray[pointer++];
    }

    bool numberParsed;
    qint64 number = QString(QLatin1String(numberPart)).toLongLong(&numberParsed);
    if (!numberParsed)
        return nullptr;

    if (pointer >= byteArray.size() || byteArray[pointer++] != 'e')
        return nullptr;

    return new BEncodingNumber(number);
}

BEncodingString *BEncodingParser::parseString()
{
    QByteArray lengthPart;
    while (pointer < byteArray.size() && byteArray[pointer] != ':' && lengthPart.length() < 20) {
        lengthPart += byteArray[pointer++];
    }
    bool lengthParsed;
    int length = QString(QLatin1String(lengthPart)).toInt(&lengthParsed);
    if (!lengthParsed)
        return nullptr;
    if (pointer >= byteArray.size() || byteArray[pointer++] != ':')
        return nullptr;
    QByteArray stringPart = byteArray.mid(pointer, length);
    if (stringPart.length() != length)
        return nullptr;

    pointer += stringPart.length();

    return new BEncodingString(stringPart);
}

}
