#include "bencoding/BEncodingNumber.h"

#include <QString>

namespace bencoding {

BEncodingNumber::BEncodingNumber(qint64 value)
    : BEncodingObject(), value(value)
{

}

BEncodingNumber::BEncodingNumber(int value)
    : BEncodingObject(), value(value)
{

}

BEncodingNumber::BEncodingNumber(qint16 value)
    : BEncodingObject(), value(value)
{

}

BEncodingNumber::BEncodingNumber(const BEncodingNumber &other)
    : BEncodingNumber(other.value)
{

}

BEncodingNumber::BEncodingNumber()
    : BEncodingNumber(0)
{

}

BEncodingObject *BEncodingNumber::deepCopy() const
{
    return new BEncodingNumber(value);
}

BEncodingNumber::~BEncodingNumber()
{

}

QByteArray BEncodingNumber::encodeToByteArray() const
{
    QByteArray result;
    result += "i";
    result += QString::number((qlonglong)value).toLatin1();
    result += "e";
    return result;
}

qint64 BEncodingNumber::getInt64Value() const
{
    return value;
}

qint16 BEncodingNumber::getInt16Value() const
{
    return value;
}

int BEncodingNumber::getIntValue() const
{
    return value;
}

void BEncodingNumber::setInt64Value(qint64 value)
{
    this->value = (qint64)value;
}

void BEncodingNumber::setInt16Value(qint16 value)
{
    this->value = value;
}

void BEncodingNumber::setIntValue(int value)
{
    this->value = value;
}

}
