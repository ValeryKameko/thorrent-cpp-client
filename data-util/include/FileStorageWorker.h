#ifndef FILESTORAGEWORKER_H
#define FILESTORAGEWORKER_H

#include "Block.h"
#include "FileStorage.h"

#include <QBitArray>
#include <QMutex>
#include <QQueue>
#include <QSharedPointer>
#include <QThread>
#include <QWaitCondition>


struct DataRequest {
    enum RequestType {
        ReadRequest,
        WriteRequest,
        VerifyRequest,
        FullVerification
    };

    RequestType type;
    Block *writeRequest;
    BlockIdentifier *readRequest;
    qint64 verifyRequest;

    int requestId;
};


class FileStorageWorker : public QThread
{
    Q_OBJECT

public:
    FileStorageWorker(QObject *parent = nullptr);
    virtual ~FileStorageWorker() override;

    int requestRead(const BlockIdentifier &identifier);
    int requestWrite(const Block &block);
    int requestVerification(qint64 pieceIndex);
    int requestFullVerification();

    QBitArray getVerifiedPieces() const;
    void setVerifiedPieces(const QBitArray &value);

    QString getErrorString() const;

    QSharedPointer<FileStorage> getStorage() const;
    void setStorage(const QSharedPointer<FileStorage> &value);

    QList<QByteArray> getPieceHashes() const;
    void setPieceHashes(const QList<QByteArray> &value);

signals:
    void dataRead(int requestId, const Block &data);
    void dataWritten(int requestId, const BlockIdentifier &identifier);
    void pieceVerified(int requestId, qint64 pieceIndex, bool result);
    void fullVerificationProgress(int requestId, int percent);
    void fullVerificationDone(int requestId);

    void error();

protected:
    virtual void run() override;

private slots:
    bool verifyPiece(qint64 pieceIndex);

private:
    void verifyFull(int requestId);

    QQueue<DataRequest> requests;
    QList<QByteArray> pieceHashes;
    QBitArray verifiedPieces;

    mutable QMutex lock;
    mutable QWaitCondition condition;

    QAtomicInt requestId;

    QString errorString;
    QSharedPointer<FileStorage> storage;

    QAtomicInt needQuit;
};

#endif // FILESTORAGEWORKER_H
