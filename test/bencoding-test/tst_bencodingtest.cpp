#include <QtTest>

#include <bencoding/BEncodingParser.h>

using namespace bencoding;

class BEncodingTest : public QObject
{
    Q_OBJECT

public:
    BEncodingTest();
    ~BEncodingTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void BEncodingParser_shouldParseNumber();
    void BEncodingParser_shouldParseString();
    void BEncodingParser_shouldParseList();
    void BEncodingParser_shouldParseDictionary();
};

BEncodingTest::BEncodingTest()
{

}

BEncodingTest::~BEncodingTest()
{

}

void BEncodingTest::initTestCase()
{

}

void BEncodingTest::cleanupTestCase()
{

}

void BEncodingTest::BEncodingParser_shouldParseNumber()
{
    QString testValue = "i12e";

    BEncodingParser parser;
    QScopedPointer<BEncodingObject> obj{};
    obj.reset(parser.parse(testValue.toLatin1()));
    QVERIFY(!obj.isNull());
    QVERIFY(obj->encodeToString() == testValue);
}

void BEncodingTest::BEncodingParser_shouldParseString()
{
    QString testValue = "6:abc de";

    BEncodingParser parser;
    QScopedPointer<BEncodingObject> obj{};
    obj.reset(parser.parse(testValue.toLatin1()));
    QVERIFY(!obj.isNull());
    QVERIFY(obj->encodeToString() == testValue);
}

void BEncodingTest::BEncodingParser_shouldParseList()
{
    QString testValue = "li1e1:ee";

    BEncodingParser parser;
    QScopedPointer<BEncodingObject> obj{};
    obj.reset(parser.parse(testValue.toLatin1()));
    QVERIFY(!obj.isNull());
    QVERIFY(obj->encodeToString() == testValue);
}

void BEncodingTest::BEncodingParser_shouldParseDictionary()
{
    QString testValue = "d1:ri65ee";

    BEncodingParser parser;
    QScopedPointer<BEncodingObject> obj{};
    obj.reset(parser.parse(testValue.toLatin1()));
    QVERIFY(!obj.isNull());
    QVERIFY(obj->encodeToString() == testValue);

}

QTEST_APPLESS_MAIN(BEncodingTest)

#include "tst_bencodingtest.moc"
