#include "bencoding/BEncodingList.h"

namespace bencoding {

BEncodingList::BEncodingList()
    : BEncodingObject (), list()
{

}

BEncodingList::BEncodingList(const BEncodingList &other)
    : BEncodingObject()
{
    for (auto element : other.list) {
        list << element->deepCopy();
    }
}

BEncodingList::BEncodingList(BEncodingObject *element)
    : BEncodingObject (), list({ element })
{
}

BEncodingList::BEncodingList(const QList<BEncodingObject *> &list)
    : BEncodingObject (), list(list)
{
}

BEncodingObject *BEncodingList::deepCopy() const
{
    BEncodingList *copy = new BEncodingList();
    for (auto element : list) {
        copy->getList() << element->deepCopy();
    }
    return copy;
}

BEncodingList::~BEncodingList()
{
    for (auto element : list) {
        delete element;
    }
    list.clear();
}

QByteArray BEncodingList::encodeToByteArray() const
{
    QByteArray result;
    result += "l";
    for (auto element : list) {
        result += element->encodeToByteArray();
    }
    result += "e";
    return result;
}

const QList<BEncodingObject*> &BEncodingList::getList() const
{
    return list;
}

QList<BEncodingObject *> &BEncodingList::getList()
{
    return list;
}

}
