QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    tst_metainfotest.cpp

CONFIG(debug, debug|release) {
    DEBUG_OR_RELEASE = debug
}  else {
    DEBUG_OR_RELEASE = release
}

resources.commands = $(COPY_DIR) $$shell_path($$PWD/resources) $$shell_path($$OUT_PWD/$${DEBUG_OR_RELEASE}/resources)
first.depends = $(first) resources
export(first.depends)
export(resources.commands)
QMAKE_EXTRA_TARGETS += first resources

DISTFILES += \
    resources/multi_info_file.torrent \
    resources/single_info_file.torrent \
    resources/test.torrent

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../metainfo/release/ -lmetainfo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../metainfo/debug/ -lmetainfo
else:unix: LIBS += -L$$OUT_PWD/../../metainfo/ -lmetainfo

INCLUDEPATH += $$PWD/../../metainfo/include
DEPENDPATH += $$PWD/../../metainfo/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../bencoding/release/ -lbencoding
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../bencoding/debug/ -lbencoding
else:unix: LIBS += -L$$OUT_PWD/../../bencoding/ -lbencoding

INCLUDEPATH += $$PWD/../../bencoding/include
DEPENDPATH += $$PWD/../../bencoding/include
