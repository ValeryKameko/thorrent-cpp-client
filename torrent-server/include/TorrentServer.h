#ifndef TORRENTSERVER_H
#define TORRENTSERVER_H

#include <PWPClient.h>
#include <QObject>
#include <QTcpServer>
#include <SendReceiveController.h>
#include <TorrentConnectionManager.h>

class TorrentServer : public QObject
{
    Q_OBJECT
public:
    explicit TorrentServer(QObject *parent = nullptr);

    void setSendReceiveController(const QSharedPointer<SendReceiveController> &controller);
    void setConnectionManager(const QSharedPointer<TorrentConnectionManager> &manager);

    void addClient(PWPClient *client);
    void removeClient(PWPClient *client);

    QTcpServer &getServerSocket();
    const QTcpServer &getServerSocket() const;

signals:

public slots:

private slots:
    void newIncomingConnection(qintptr socketFd);

private:
    QTcpServer server;
    QSharedPointer<SendReceiveController> controller;
    QSharedPointer<TorrentConnectionManager> manager;

    QList<TorrentClient*> clients;
};

#endif // TORRENTSERVER_H
