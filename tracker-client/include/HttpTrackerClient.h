#ifndef HTTPTRACKERCLIENT_H
#define HTTPTRACKERCLIENT_H

#include "TrackerClient.h"

#include <QNetworkAccessManager>
#include <QObject>

class HttpTrackerClient : public TrackerClient
{
    Q_OBJECT
public:
    HttpTrackerClient(QObject *parent = nullptr);
    virtual ~HttpTrackerClient() override;

    void setAnnounceUrl(const QUrl &url);

    virtual void setComplete(bool isCompleted);
    virtual void start() override;
    virtual void stop() override;

protected:
    virtual void timerEvent(QTimerEvent * event) override;

protected slots:
    virtual void announce() override;

private slots:
    void provideAuthentification(QNetworkReply *reply, QAuthenticator *authenticator);
    void handleResponse(QNetworkReply *reply);

private:
    QByteArray percentEncode(const QByteArray &data);

    bool firstRequest;
    bool lastRequest;
    bool isCompleted;
    bool isWorking;

    QUrl url;
    QByteArray trackerId;

    QString userName;
    QString userPassword;

    QNetworkAccessManager httpClient;

    int requestTimerId;
    int requestTimerInterval;

};

#endif // HTTPTRACKERCLIENT_H
