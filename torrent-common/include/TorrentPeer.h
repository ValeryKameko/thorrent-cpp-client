#ifndef TORRENTPEER_H
#define TORRENTPEER_H

#include <QBitArray>
#include <QHostAddress>



class TorrentPeer
{
public:
    TorrentPeer();

    bool operator=(const TorrentPeer &other) const;

    bool getInteresting() const;
    void setInteresting(bool value);

    QBitArray getPiecesHave() const;
    void setPiecesHave(const QBitArray &value);

    QHostAddress getAddress() const;
    void setAddress(const QHostAddress &value);

    int getPort() const;
    void setPort(int value);

    QByteArray getPeerId() const;
    void setPeerId(const QByteArray &value);

    uint getLastVisited() const;
    void setLastVisited(const uint &value);

    uint getConnectStart() const;
    void setConnectStart(const uint &value);

    int getNumCompletedPieces() const;
    void setNumCompletedPieces(int value);

    uint getConnectTime() const;
    void setConnectTime(const uint &value);

    bool getSeed() const;
    void setSeed(bool value);

private:
    bool interesting;
    bool seed;
    QBitArray piecesHave;

    QHostAddress address;
    int port;
    QByteArray peerId;

    uint lastVisited;
    uint connectStart;
    uint connectTime;

    int numCompletedPieces;
};

#endif // TORRENTPEER_H
