#include "include/TorrentPeer.h"

TorrentPeer::TorrentPeer()
{

}

bool TorrentPeer::operator=(const TorrentPeer &other) const
{
    return other.port == port &&
            other.address == address &&
            other.peerId == peerId;
}

bool TorrentPeer::getInteresting() const
{
    return interesting;
}

void TorrentPeer::setInteresting(bool value)
{
    interesting = value;
}

QBitArray TorrentPeer::getPiecesHave() const
{
    return piecesHave;
}

void TorrentPeer::setPiecesHave(const QBitArray &value)
{
    piecesHave = value;
}

QHostAddress TorrentPeer::getAddress() const
{
    return address;
}

void TorrentPeer::setAddress(const QHostAddress &value)
{
    address = value;
}

int TorrentPeer::getPort() const
{
    return port;
}

void TorrentPeer::setPort(int value)
{
    port = value;
}

QByteArray TorrentPeer::getPeerId() const
{
    return peerId;
}

void TorrentPeer::setPeerId(const QByteArray &value)
{
    peerId = value;
}

uint TorrentPeer::getLastVisited() const
{
    return lastVisited;
}

void TorrentPeer::setLastVisited(const uint &value)
{
    lastVisited = value;
}

uint TorrentPeer::getConnectStart() const
{
    return connectStart;
}

void TorrentPeer::setConnectStart(const uint &value)
{
    connectStart = value;
}

int TorrentPeer::getNumCompletedPieces() const
{
    return numCompletedPieces;
}

void TorrentPeer::setNumCompletedPieces(int value)
{
    numCompletedPieces = value;
}

uint TorrentPeer::getConnectTime() const
{
    return connectTime;
}

void TorrentPeer::setConnectTime(const uint &value)
{
    connectTime = value;
}

bool TorrentPeer::getSeed() const
{
    return seed;
}

void TorrentPeer::setSeed(bool value)
{
    seed = value;
}
