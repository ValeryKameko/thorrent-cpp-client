#include "include/TrackerClient.h"

TrackerClient::TrackerClient(QObject *parent)
    : QObject(parent), clientState(nullptr), globalState(nullptr)
{

}

TrackerClient::~TrackerClient()
{

}

void TrackerClient::setTorrentClientState(TorrentClientState *clientState)
{
    this->clientState = clientState;
}

void TrackerClient::setTorrentGlobalState(TorrentGlobalState *globalState)
{
    this->globalState = globalState;
}

QString TrackerClient::getErrorString() const
{
    return errorString;
}

QString TrackerClient::getWarningString() const
{
    return warningString;
}
