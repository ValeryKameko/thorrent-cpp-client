#include "bencoding/BEncodingDictionary.h"

namespace bencoding {

BEncodingDictionary::BEncodingDictionary(const QMap<BEncodingString, BEncodingObject *> &map)
    : BEncodingObject (), map(map)
{

}


BEncodingDictionary::BEncodingDictionary()
    : BEncodingObject (), map()
{

}

BEncodingDictionary::~BEncodingDictionary()
{
    for(auto element : map.values()) {
        delete element;
    }
    map.clear();
}

BEncodingObject *BEncodingDictionary::deepCopy() const
{
    BEncodingDictionary *copy = new BEncodingDictionary();
    for (auto it = map.begin(); it != map.end(); it++) {
        copy->map.insert(BEncodingString(it.key()),
                         it.value()->deepCopy());
    }
    return copy;
}

QByteArray BEncodingDictionary::encodeToByteArray() const
{
    QByteArray result;
    result += "d";
    for (auto it = map.begin(); it != map.end(); it++) {
        result += it.key().encodeToByteArray();
        result += it.value()->encodeToByteArray();
    }
    result += "e";

    return result;
}

const QMap<BEncodingString, BEncodingObject *> &BEncodingDictionary::getMap() const
{
    return map;
}

QMap<BEncodingString, BEncodingObject *> &BEncodingDictionary::getMap()
{
    return map;
}

}
