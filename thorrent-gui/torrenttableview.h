#ifndef TORRENTTABLEVIEW_H
#define TORRENTTABLEVIEW_H

#include <QWidget>
#include <TorrentClient.h>

struct TorrentRow {
    TorrentClient *client;
    QString name;
};

namespace Ui {
class TorrentTableView;
}

class TorrentTableView : public QWidget
{
    Q_OBJECT

public:
    explicit TorrentTableView(QWidget *parent = nullptr);
    ~TorrentTableView();

    void addTorrent(const QString &filePath, const QString &destinationFolder);
    void deleteTorrent();
    void pauseTorrent();

private slots:
    void torrentStateChanged(TorrentClientState::TorrentClientWorkingState state);
    void torrentProgressUpdated(qint64 progress);
    void torrentDownloadSpeedUpdated(qint64 bytesPerSecond);
    void torrentUploadSpeedUpdated(qint64 bytesPerSecond);
    void torrentStopped();

private:
    void stopTorrent(TorrentRow *row);
    void updateRow(int rowIndex);
    TorrentRow *getTorrentRowByClient(TorrentClient *client);
    int getTorrentRowIndex(TorrentRow *row);

    Ui::TorrentTableView *ui;

    QList<TorrentRow *> torrents;
};

#endif // TORRENTTABLEVIEW_H
