#ifndef PWPCONNECTIONSTATE_H
#define PWPCONNECTIONSTATE_H

#include <QByteArray>
#include <QBitArray>
#include <BlockIdentifier.h>
#include <Block.h>
#include <QQueue>
#include <QObject>


class TorrentPeer;

struct BlockInfo {
    int pieceIndex;
    int offset;
    int length;
    QByteArray block;
};


class PWPConnectionState : public QObject
{
    Q_OBJECT
public:
    enum PWPStateFlag {
        Choked = 0x1,
        Interested = 0x2,
    };

    Q_DECLARE_FLAGS(PWPState, PWPStateFlag)

    PWPConnectionState(QObject *parent = nullptr);

    QQueue<BlockIdentifier> &getIncomingBlockRequests();
    const QQueue<BlockInfo> &getPendingBlocks() const ;
    QQueue<BlockInfo> &getPendingBlocks();
    qint64 getPendingBlocksSize();
    void setPendingBlocksSize(const qint64 &value);

    PWPState getSelf() const;
    void setSelf(const PWPState &value);

    PWPState getOther() const;
    void setOther(const PWPState &value);

    bool getRecevedHandshake() const;
    void setRecevedHandshake(bool value);

    bool getSentHandshake() const;
    void setSentHandshake(bool value);

    bool getGotPeerId() const;
    void setGotPeerId(bool value);

    QByteArray getSelfPeerId() const;
    void setSelfPeerId(const QByteArray &value);

    TorrentPeer *getTorrentPeer() const;
    void setTorrentPeer(TorrentPeer *value);

    QByteArray getInfoHash() const;
    void setInfoHash(const QByteArray &value);

    QBitArray &getPeerPieces();

    QByteArray getOtherPeerId() const;
    void setOtherPeerId(const QByteArray &value);

private:
    QQueue<BlockIdentifier> incomingBlockRequests;
    QQueue<BlockInfo> pendingBlocks;
    qint64 pendingBlocksSize;

    PWPState self;
    PWPState other;

    bool recevedHandshake;
    bool sentHandshake;
    bool gotPeerId;

    QByteArray infoHash;
    QByteArray selfPeerId;
    QByteArray otherPeerId;
    QBitArray peerPieces;

    TorrentPeer *torrentPeer;
};

#endif // PWPCONNECTIONSTATE_H
