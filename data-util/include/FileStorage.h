#ifndef FILESTORAGE_H
#define FILESTORAGE_H

#include "Block.h"

#include <QFile>
#include <QList>
#include <QMutex>
#include <QString>


class FileEntry {
public:
    FileEntry(qint64 fileSize,
              const QByteArray &md5Hash,
              const QString &relativePath);

    const QString &getRelativePath() const;
    QByteArray getMd5Hash() const;
    qint64 getFileSize() const;

private:
    qint64 fileSize;
    QByteArray md5Hash;
    QString relativePath;
};

class FileStorage : public QObject
{
    Q_OBJECT
public:
    FileStorage(QObject *parent = nullptr);
    virtual ~FileStorage();

    void setFolder(const QString &folder);
    QString getFolder() const;

    void setEntries(const QList<FileEntry> &entries);
    void setPieceLength(qint64 pieceLength);

    Block *readBlock(const BlockIdentifier &identifier);
    bool writeBlock(const Block &block);

    bool createStorage();

    const QString &getErrorString() const;

    qint64 getPieceLengthAt(qint64 pieceIndex) const;
    qint64 getPiecesCount() const;
    qint64 getTotalSize() const;


private:
    mutable QMutex lock;

    QString folder;
    QList<FileEntry> entries;
    QList<QSharedPointer<QFile>> files;

    QString errorString;
    qint64 totalSize;
    qint64 pieceLength;

    bool needQuit;
};

#endif // FILESTORAGE_H
