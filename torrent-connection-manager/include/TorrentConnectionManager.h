#ifndef TORRENTCONNECTIONMANAGER_H
#define TORRENTCONNECTIONMANAGER_H

#include <QByteArray>
#include <QObject>
#include <QSet>

class PWPClient;

class TorrentConnectionManager : public QObject
{
    Q_OBJECT
public:
    static TorrentConnectionManager *getInstance();

    bool canAddClient();
    int getClientLimit() const;
    void addClient(PWPClient *client);
    void removeClient(PWPClient *client);
    QByteArray generatePeerId();
private:
    TorrentConnectionManager();

    QSet<PWPClient *> peers;
    QByteArray peerId;
};

#endif // TORRENTCONNECTIONMANAGER_H
