#ifndef TORRENTCLIENT_H
#define TORRENTCLIENT_H

#include <QObject>
#include <QHostAddress>
#include <TorrentClientState.h>
#include <TorrentGlobalState.h>
#include <TorrentPeer.h>
#include <Block.h>
#include <BlockIdentifier.h>
#include <metainfo/Metainfo.h>

class PWPClient;
class TorrentPiece;
class TorrentClientPrivate;

class TorrentClient : public QObject
{
    Q_OBJECT

public:
    TorrentClient(QObject *parent = nullptr);
    ~TorrentClient();

    bool setTorrent(const QString &fileName);
    bool setTorrent(const QByteArray &torrentData);

    qint64 getProgress() const;
    int getConnectedPeerCount() const;
    int getSeedCount() const;

    TorrentClientState &getState();

    TorrentGlobalState *getGlobalState() const;
    void setGlobalState(TorrentGlobalState *value);

    bool getPaused() const;

signals:
    void stateChanged(TorrentClientState::TorrentClientWorkingState state);
    void errorFired(TorrentClientState::TorrentClientError error);

    void peerInfoUpdated();
    void downloadCompleted();

    void dataSent(qint64 uploadedBytes);
    void dataReceived(qint64 downloadedBytes);
    void progressUpdated(qint64 percentProgress);
    void downloadRateUpdated(qint64 bytesPerSecond);
    void uploadRateUpdated(qint64 bytesPerSecond);

    void stopped();
    void completed();

public slots:
    void start();
    void stop();
    void setPaused(bool paused);
    void setupIncomingConnection(PWPClient *client);

protected slots:
    void timerEvent(QTimerEvent *event) override;

private slots:
    void sendBlockToPeer(int requestId, const Block &block);
    void fullVerificationDone(int requestId);
    void pieceVerified(int requestId, qint64 pieceIndex, bool result);
    void handleFileError();

    void connectToPeers();
    void setupOutgoingConnection();
    void initializeConnection(PWPClient *client);
    void removeClient();
    void peerUpdateAvailable(const QBitArray &pieces);
    void peerRequestsBlock(const BlockIdentifier &identifier);
    void blockReceived(const Block &block);

    void pwpBytesWritten(qint64 bytes);
    void pwpBytesReceived(qint64 bytes);

    int blocksLeftForPiece(const TorrentPiece *piece) const;

    void scheduleUploads();
    void scheduleDownloads();
    void schedulePieceForClient(PWPClient *client);
    void requestMore(PWPClient *client);
    int requestBlocks(PWPClient *client, TorrentPiece *piece, int maxBlocks);
    void peerChoked();
    void peerUnchoked();

    void addToPeerList(const QList<TorrentPeer> &peerList);
    void trackerStopped();

    void updateProgress(int requestId, int getProgress = -1);

private:
    TorrentClientState state;
    TorrentGlobalState *globalState;
    TorrentClientPrivate *impl;
    friend class TorrentClientPrivate;
};

#endif // TORRENTCLIENT_H
