#-------------------------------------------------
#
# Project created by QtCreator 2019-06-09T01:55:28
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = torrent-server
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/TorrentServer.cpp

HEADERS += \
    include/TorrentServer.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pwp-client/release/ -lpwp-client
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pwp-client/debug/ -lpwp-client
else:unix: LIBS += -L$$OUT_PWD/../pwp-client/ -lpwp-client

INCLUDEPATH += $$PWD/../pwp-client/include
DEPENDPATH += $$PWD/../pwp-client/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pwp-client/release/libpwp-client.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pwp-client/debug/libpwp-client.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pwp-client/release/pwp-client.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pwp-client/debug/pwp-client.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../pwp-client/libpwp-client.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../send-receive-controller/release/ -lsend-receive-controller
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../send-receive-controller/debug/ -lsend-receive-controller
else:unix: LIBS += -L$$OUT_PWD/../send-receive-controller/ -lsend-receive-controller

INCLUDEPATH += $$PWD/../send-receive-controller/include
DEPENDPATH += $$PWD/../send-receive-controller/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../send-receive-controller/release/libsend-receive-controller.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../send-receive-controller/debug/libsend-receive-controller.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../send-receive-controller/release/send-receive-controller.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../send-receive-controller/debug/send-receive-controller.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../send-receive-controller/libsend-receive-controller.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/release/ -ltorrent-connection-manager
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../torrent-connection-manager/debug/ -ltorrent-connection-manager
else:unix: LIBS += -L$$OUT_PWD/../torrent-connection-manager/ -ltorrent-connection-manager

INCLUDEPATH += $$PWD/../torrent-connection-manager/include
DEPENDPATH += $$PWD/../torrent-connection-manager/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../torrent-connection-manager/release/libtorrent-connection-manager.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../torrent-connection-manager/debug/libtorrent-connection-manager.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../torrent-connection-manager/release/torrent-connection-manager.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../torrent-connection-manager/debug/torrent-connection-manager.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../torrent-connection-manager/libtorrent-connection-manager.a
