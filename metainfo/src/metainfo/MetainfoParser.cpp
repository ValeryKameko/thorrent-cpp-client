#include "metainfo/MetainfoParser.h"

#include <QDebug>

using namespace bencoding;

namespace metainfo {


static constexpr int PIECE_SIZE = 20;

MetainfoParser::MetainfoParser()
    : fileEntry(0, QByteArray(), "")
{
}

Metainfo *MetainfoParser::parse(const QByteArray &byteArray)
{
    dict = bencodingParser.parseDictionary(byteArray);
    if (!dict)
        return nullptr;

    builder = MetainfoBuilder();
    infoBuilder = MetainfoInfoBuilder();
    fileEntry = MetainfoInfoFileEntry(0, QByteArray(), "");

    bool isParsed =
            parseInfo(getDictionaryValue<BEncodingDictionary>(dict, "info")) &&
            parseCreatedBy(getDictionaryValue<BEncodingString>(dict, "created by")) &&
            parseCreatedDate(getDictionaryValue<BEncodingNumber>(dict, "creation date")) &&
            parseComment(getDictionaryValue<BEncodingString>(dict, "comment")) &&
            parseAnnounce(getDictionaryValue<BEncodingString>(dict, "announce")) &&
            parseAnnounceList(getDictionaryValue<BEncodingList>(dict, "announce-list"));
    if (!isParsed) {
        qDebug() << "Metainfo parse error";
        return nullptr;
    }

    Metainfo *metainfo = new Metainfo(builder.build());
    qDebug() << "Metainfo for " << metainfo->getInfo().getName() << "parsed sucessfully";
    return metainfo;
}

void MetainfoParser::clear()
{

}

bool MetainfoParser::parseAnnounce(BEncodingString *string)
{
    if (!string)
        return false;
    QUrl url = QUrl::fromEncoded(string->getValue());
    if (!url.isValid())
        return false;
    builder.setAnnounceTracker(url);
    return true;
}

bool MetainfoParser::parseAnnounceList(BEncodingList *list)
{
    if (!list)
        return true;
    QList<QList<QUrl>> announceList;
    for (auto entry : list->getList()) {
        auto entryList = dynamic_cast<BEncodingList *>(entry);
        if (!entryList)
            return false;

        QList<QUrl> announceListEntry;
        for (auto entry : entryList->getList()) {
            auto entryString = dynamic_cast<BEncodingString *>(entry);
            if (!entryString)
                return false;
            QUrl url = QUrl::fromEncoded(entryString->getValue());
            if (!url.isValid())
                continue;
            announceListEntry.append(url);
        }
        announceList.append(announceListEntry);
    }
    builder.setAnnounceTrackerList(announceList);
    return true;
}

bool MetainfoParser::parseComment(BEncodingString *string)
{
    if (!string)
        return true;
    builder.setComment(QString::fromUtf8(string->getValue()));
    return true;
}

bool MetainfoParser::parseCreatedBy(BEncodingString *string)
{
    if (!string)
        return true;
    builder.setCreatedBy(QString::fromUtf8(string->getValue()));
    return true;
}

bool MetainfoParser::parseCreatedDate(BEncodingNumber *number)
{
    if (!number)
        return true;
    builder.setCreationDate(QDateTime::fromSecsSinceEpoch(number->getInt64Value()));
    return true;
}

bool MetainfoParser::parseInfo(BEncodingDictionary *dictionary)
{
    if (getDictionaryValue<BEncodingList>(dictionary, "files")) {
        bool isParsed =
                parseFiles(getDictionaryValue<BEncodingList>(dictionary, "files")) &&
                parseName(getDictionaryValue<BEncodingString>(dictionary, "name")) &&
                parsePieces(getDictionaryValue<BEncodingString>(dictionary, "pieces")) &&
                parsePieceLength(getDictionaryValue<BEncodingNumber>(dictionary, "piece length"));
        if (!isParsed) {
            qDebug() << "Metainfo parse multifile error";
            return false;
        }
    } else {
        bool isParsed =
                parseMd5Hash(getDictionaryValue<BEncodingString>(dictionary, "md5sum")) &&
                parseLength(getDictionaryValue<BEncodingNumber>(dictionary, "length")) &&
                parseName(getDictionaryValue<BEncodingString>(dictionary, "name")) &&
                parsePieces(getDictionaryValue<BEncodingString>(dictionary, "pieces")) &&
                parsePieceLength(getDictionaryValue<BEncodingNumber>(dictionary, "piece length"));
        if (!isParsed) {
            qDebug() << "Metainfo parse singlefile error";
            return false;
        }
        fileEntry.relativePath = infoBuilder.getName();
        infoBuilder.addFileEntry(fileEntry);
    }
    infoBuilder.setInfoData(dictionary->encodeToByteArray());

    builder.setInfo(infoBuilder.build());
    return true;
}

bool MetainfoParser::parseName(BEncodingString *string)
{
    if (!string) {
        qDebug() << "Metainfo parse name error";
        return false;
    }
    infoBuilder.setName(QString::fromUtf8(string->getValue()));
    return true;
}

bool MetainfoParser::parsePath(BEncodingList *list)
{
    if (!list) {
        qDebug() << "Metainfo parse path error";
        return false;
    }
    QByteArray path;
    for (auto entry : list->getList()) {
        auto entryString = dynamic_cast<BEncodingString *>(entry);
        if (!entryString) {
            qDebug() << "Metainfo parse path error";
            return false;
        }
        path += entryString->getValue();
    }
    fileEntry.relativePath = QString::fromUtf8(path);
    return true;

}

bool MetainfoParser::parseFiles(BEncodingList *list)
{
    if (!list) {
        qDebug() << "Metainfo parse files error";
        return false;
    }

    QList<MetainfoInfoFileEntry> files;
    for (auto entry : list->getList()) {
        auto entryDict = dynamic_cast<BEncodingDictionary *>(entry);
        if (!entryDict) {
            qDebug() << "Metainfo parse files error";
            return false;
        }

        bool isParsed =
                parsePath(getDictionaryValue<BEncodingList>(entryDict, "path")) &&
                parseLength(getDictionaryValue<BEncodingNumber>(entryDict, "length")) &&
                parseMd5Hash(getDictionaryValue<BEncodingString>(entryDict, "md5sum"));
        if (!isParsed) {
            qDebug() << "Metainfo parse files error";
            return false;
        }
        files.append(fileEntry);
    }

    infoBuilder.setFileEntries(files);
    return true;
}

bool MetainfoParser::parsePieces(BEncodingString *string)
{
    if (!string || (string->getValue().size() % PIECE_SIZE != 0)) {
        qDebug() << "Metainfo parse pieces error";
        return false;
    }
    QList<QByteArray> pieces;
    for (int i = 0; i < string->getValue().size(); i += PIECE_SIZE) {
        pieces.append(string->getValue().mid(i, PIECE_SIZE));
    }
    infoBuilder.setPieceHashes(pieces);
    return true;
}

bool MetainfoParser::parsePieceLength(BEncodingNumber *number)
{
    if (!number) {
        qDebug() << "Metainfo parse piece length error";
        return false;
    }
    infoBuilder.setPieceLength(number->getIntValue());
    return true;
}

bool MetainfoParser::parseLength(BEncodingNumber *number)
{
    if (!number) {
        qDebug() << "Metainfo parse length error";
        return false;
    }
    fileEntry.fileSize = number->getInt64Value();
    return true;
}

bool MetainfoParser::parseMd5Hash(BEncodingString *string)
{
    if (!string)
        return true;
    fileEntry.md5Hash = string->getValue();
    return true;
}

template<class T>
T *MetainfoParser::getDictionaryValue(BEncodingDictionary *dictionary, const QString &key)
{
    if (!dictionary)
        return nullptr;
    BEncodingObject *obj = dictionary->getMap().value(key);
    if (!obj)
        return nullptr;
    return dynamic_cast<T*>(obj);
}

}
