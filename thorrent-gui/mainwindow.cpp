#include "addtorrentdialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "uicommon.h"

#include <QLabel>
#include <QSpacerItem>
#include <SendReceiveController.h>

static const int SLIDER_MIN = 1;
static const int SLIDER_MAX = 100;

static const int UPLOAD_SPEED_MIN = 1024;
static const int UPLOAD_VALUE = 50;
static const int UPLOAD_SPEED_MAX = 1024 * 1024;

static const int DOWNLOAD_SPEED_MIN = 1024;
static const int DOWNLOAD_VALUE = 50;
static const int DOWNLOAD_SPEED_MAX = 1024* 1024;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->bottomToolBar->setContentsMargins(10, 0, 10, 0);

    downloadSpeedLabel = new QLabel(UICommon::formatSize(0), this);
    uploadSpeedLabel = new QLabel(UICommon::formatSize(0), this);
    createSliders();

    ui->bottomToolBar->addWidget(new QLabel("Download speed: ", this));
    ui->bottomToolBar->addWidget(downloadSpeedSlider);
    ui->bottomToolBar->addWidget(downloadSpeedLabel);

    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);

    ui->bottomToolBar->addWidget(spacer);
    ui->bottomToolBar->addWidget(new QLabel("Upload speed: ", this));
    ui->bottomToolBar->addWidget(uploadSpeedSlider);
    ui->bottomToolBar->addWidget(uploadSpeedLabel);

    torrentTableView = new TorrentTableView(this);
    setCentralWidget(torrentTableView);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAdd_Torrent_triggered()
{
    AddTorrentDialog *dialog = new AddTorrentDialog(this);
    if (!dialog->exec())
        return;

    addTorrent(dialog->getTorrentFilePath(), dialog->getDestinationFolder());
    dialog->deleteLater();
}

void MainWindow::downloadSliderValueChanged(int value)
{
    qint64 downloadSpeed = ((value - SLIDER_MIN) * (DOWNLOAD_SPEED_MAX - DOWNLOAD_SPEED_MIN)) / (SLIDER_MAX - SLIDER_MIN) + DOWNLOAD_SPEED_MIN;
    downloadSpeedLabel->setText(UICommon::formatSize(downloadSpeed));
    SendReceiveController::getInstance().setDownloadRate(downloadSpeed);
}

void MainWindow::uploadSliderValueChanged(int value)
{
    qint64 uploadSpeed = ((value - SLIDER_MIN) * (UPLOAD_SPEED_MAX - UPLOAD_SPEED_MIN)) / (SLIDER_MAX - SLIDER_MIN) + UPLOAD_SPEED_MIN;
    uploadSpeedLabel->setText(UICommon::formatSize(uploadSpeed));
    SendReceiveController::getInstance().setUploadRate(uploadSpeed);
}

void MainWindow::addTorrent(const QString &filePath, const QString &destinationFolder)
{
    torrentTableView->addTorrent(filePath, destinationFolder);
}

void MainWindow::createSliders()
{
    downloadSpeedSlider = new QSlider(Qt::Orientation::Horizontal, this);
    downloadSpeedSlider->setRange(SLIDER_MIN, SLIDER_MAX);
    QObject::connect(downloadSpeedSlider, SIGNAL(valueChanged(int)),
                     this, SLOT(downloadSliderValueChanged(int)));
    downloadSpeedSlider->setTickInterval(10);
    downloadSpeedSlider->setTickPosition(QSlider::TickPosition::NoTicks);
    downloadSpeedSlider->setValue(DOWNLOAD_VALUE);

    uploadSpeedSlider = new QSlider(Qt::Orientation::Horizontal, this);
    uploadSpeedSlider->setRange(SLIDER_MIN, SLIDER_MAX);
    QObject::connect(uploadSpeedSlider, SIGNAL(valueChanged(int)),
                     this, SLOT(uploadSliderValueChanged(int)));
    uploadSpeedSlider->setTickInterval(10);
    uploadSpeedSlider->setTickPosition(QSlider::TickPosition::NoTicks);
    uploadSpeedSlider->setValue(UPLOAD_VALUE);
}

void MainWindow::on_actionStop_Torrent_triggered()
{
    torrentTableView->deleteTorrent();
}

void MainWindow::on_actionPause_Torrent_triggered()
{
    torrentTableView->pauseTorrent();
}
