#include "include/FileStorage.h"

#include <QDir>
#include <QSharedPointer>
#include <QDebug>


QByteArray FileEntry::getMd5Hash() const
{
    return md5Hash;
}

qint64 FileEntry::getFileSize() const
{
    return fileSize;
}

FileEntry::FileEntry(qint64 fileSize, const QByteArray &md5Hash, const QString &relativePath)
    : fileSize(fileSize), md5Hash(md5Hash), relativePath(relativePath)
{

}

const QString &FileEntry::getRelativePath() const
{
    return relativePath;
}

FileStorage::FileStorage(QObject *parent)
    : QObject(parent)
{

}

FileStorage::~FileStorage()
{
    for (auto file : files) {
        file->close();
    }

    qDebug() << "Destroyed"
             << " FileStorage " << folder;
}

void FileStorage::setFolder(const QString &folder)
{
    this->folder = folder;
}

void FileStorage::setEntries(const QList<FileEntry> &entries)
{
    this->entries = entries;

    totalSize = 0;
    for (auto entry : entries) {
        totalSize += entry.getFileSize();
    }
}

void FileStorage::setPieceLength(qint64 pieceLength)
{
    this->pieceLength = pieceLength;
}

Block *FileStorage::readBlock(const BlockIdentifier &identifier)
{
    QMutexLocker locker(&lock);
    QByteArray data;
    qint64 needLeft = identifier.getPieceIndex() * pieceLength + identifier.getOffset();
    qint64 needRight = needLeft + identifier.getLength();
    qint64 currentOffset = 0;

    qDebug() << "Reading block(" << identifier.getPieceIndex()
             << ", " << identifier.getOffset()
             << ", " << identifier.getLength() << ")"
             << " FileStorage " << folder;

    for (int i = 0; i < entries.size(); i++) {
        const auto &entry = entries.at(i);
        QSharedPointer<QFile> file = files.at(i);

        qint64 left = currentOffset;
        qint64 right = currentOffset + entry.getFileSize();
        left = qMax(left, needLeft);
        right = qMin(right, needRight);

        if (left >= right) {
            currentOffset += entry.getFileSize();
            continue;
        }

        if (!file->isOpen() && !file->open(QFile::ReadWrite)) {
            errorString = QString("Failed to access file %1: %2")
                    .arg(file->fileName())
                    .arg(file->errorString());
            qDebug() << "Read block(" << identifier.getPieceIndex()
                     << ", " << identifier.getOffset()
                     << ", " << identifier.getLength() << ") ERROR"
                     << " on open file " << file->fileName()
                     << " FileStorage " << folder;
            return nullptr;
        }

        file->seek(left - currentOffset);
        QByteArray chunk = file->read(right - left);
        file->close();

        qDebug() << "Reading element (" << (left - currentOffset)
                 << ", " << (right - left) << ") but " << chunk
                 << " FileStorage " << folder;

        if (chunk.length() != (right - left)) {
            errorString = QString("Failed to read from file %1 (read %2 bytes): %3")
                    .arg(file->fileName())
                    .arg(data.size())
                    .arg(file->errorString());
            qDebug() << "Reading block(" << identifier.getPieceIndex()
                     << ", " << identifier.getOffset()
                     << ", " << identifier.getLength() << ") ERROR"
                     << " on file " << file->fileName()
                     << " FileStorage " << folder;
            return nullptr;
        }

        data += chunk;
        currentOffset += entry.getFileSize();
    }

    return new Block(identifier.getLength(), identifier.getOffset(), data);
}

bool FileStorage::writeBlock(const Block &block)
{
    QMutexLocker locker(&lock);
    qint64 needLeft = block.getIdentifier().getPieceIndex() * pieceLength + block.getIdentifier().getOffset();
    qint64 needRight = needLeft + block.getIdentifier().getLength();
    qint64 currentOffset = 0;
    qDebug() << "Writing block(" << block.getIdentifier().getPieceIndex()
             << ", " << block.getIdentifier().getOffset()
             << ", " << block.getIdentifier().getLength() << ")"
             << " FileStorage " << folder;

    qint64 writingOffset = 0;
    for (int i = 0; i < entries.size(); i++) {
        const auto &entry = entries.at(i);
        QSharedPointer<QFile> file = files.at(i);

        qint64 left = currentOffset;
        qint64 right = currentOffset + entry.getFileSize();
        left = qMax(left, needLeft);
        right = qMin(right, needRight);

        if (left >= right) {
            currentOffset += entry.getFileSize();
            continue;
        }

        if (!file->isOpen() && !file->open(QFile::ReadWrite)) {
            errorString = QString("Failed to access file %1: %2")
                    .arg(file->fileName())
                    .arg(file->errorString());
            qDebug() << "Writing block(" << block.getIdentifier().getPieceIndex()
                     << ", " << block.getIdentifier().getOffset()
                     << ", " << block.getIdentifier().getLength() << ") ERROR"
                     << " on open file " << file->fileName() << " " << errorString
                     << " FileStorage " << folder;
            return false;
        }

        qDebug() << "Start writing block(" << block.getIdentifier().getPieceIndex()
                 << ", " << block.getIdentifier().getOffset()
                 << ", " << block.getIdentifier().getLength() << ")"
                 << " (" << left << " - " << right << ")"
                 << " to " << file->fileName()
                 << " FileStorage " << folder;

        file->seek(left - currentOffset);
        qint64 written = file->write(block.getData().constData() + writingOffset,
                                     right - left);
        file->close();

        qDebug() << "Writing element (" << (left - currentOffset)
                 << ", " << (right - left) << ") but " << written
                 << " FileStorage " << folder;

        if (written != (right - left)) {
            errorString = QString("Failed to write from file %1: %2")
                    .arg(file->fileName())
                    .arg(file->errorString());
            qDebug() << "Writing block(" << block.getIdentifier().getPieceIndex()
                     << ", " << block.getIdentifier().getOffset()
                     << ", " << block.getIdentifier().getLength() << ") ERROR"
                     << " on file " << file->fileName()
                     << " FileStorage " << folder;
            return false;
        }

        currentOffset += entry.getFileSize();
        writingOffset += right - left;
    }

    return true;
}

bool FileStorage::createStorage()
{
    QMutexLocker locker(&lock);
    QDir dir;
    QString prefix;

    qDebug() << "Creating storage "
             << " FileStorage " << folder;

    errorString = QString("Failed to create directory %1").arg(prefix);

    if (!folder.isEmpty()) {
        prefix = folder;
        if (!prefix.endsWith('/'))
            prefix += '/';
        if (!dir.mkpath(prefix)) {
            errorString = QString("Failed to create directory %1").arg(prefix);
            return false;
        }
    }

    for (auto entry : entries) {
        QSharedPointer<QFile> file(new QFile(prefix + entry.getRelativePath()));
        if (!file->open(QFile::ReadWrite)) {
            errorString = QString("Failed to access file %1: %2")
                    .arg(prefix)
                    .arg(file->errorString());
            qDebug() << "Creating file " << file->fileName()
                     << " ERROR"
                     << " FileStorage " << folder;
            return false;
        }

        if (file->size() != entry.getFileSize() && !file->resize(entry.getFileSize())) {
            errorString = QString("Failed to access file %1: %2")
                    .arg(prefix)
                    .arg(file->errorString());
            qDebug() << "Resize file " << file->fileName()
                     << " ERROR"
                     << " FileStorage " << folder;
            return false;
        }

        files << file;
        file->close();
    }
    qDebug() << "Storage created"
             << " FileStorage " << folder;
    return true;
}

qint64 FileStorage::getPieceLengthAt(qint64 pieceIndex) const
{
    QMutexLocker locker(&lock);
    qint64 left = pieceIndex * pieceLength;
    qint64 right = left + pieceLength;
    right = qMin(right, totalSize);
    return right - left;
}

qint64 FileStorage::getPiecesCount() const
{
    return (totalSize + pieceLength - 1) / pieceLength;
}

qint64 FileStorage::getTotalSize() const
{
    return totalSize;
}

QString FileStorage::getFolder() const
{
    return folder;
}

