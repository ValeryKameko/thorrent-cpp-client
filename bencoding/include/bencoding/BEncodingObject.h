#ifndef BENCODINGOBJECT_H
#define BENCODINGOBJECT_H

#include <QByteArray>

namespace bencoding {

class BEncodingObject
{
public:
    BEncodingObject();
    virtual ~BEncodingObject();

    virtual BEncodingObject * deepCopy() const = 0;

    virtual QByteArray encodeToByteArray() const = 0;
    virtual QString encodeToString() const;

};

}

#endif // BENCODINGOBJECT_H
