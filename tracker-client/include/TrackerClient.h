#ifndef TRACKERCLIENT_H
#define TRACKERCLIENT_H

#include <TorrentClientState.h>
#include <TorrentGlobalState.h>
#include <TorrentPeer.h>

#include <QObject>



class TrackerClient : public QObject
{
    Q_OBJECT
public:
    TrackerClient(QObject *parent = nullptr);
    virtual ~TrackerClient();

    void setTorrentClientState(TorrentClientState *clientState);
    void setTorrentGlobalState(TorrentGlobalState *globalState);

    virtual void start() = 0;
    virtual void stop() = 0;

    QString getErrorString() const;
    QString getWarningString() const;

signals:
    void error();
    void warning();

    void peerListUpdated(const QList<TorrentPeer> &peerList);

    void started();
    void stopped();

protected slots:
    virtual void announce() = 0;

protected:
    TorrentClientState *clientState;
    TorrentGlobalState *globalState;

    QList<TorrentPeer> peers;

    QString errorString;
    QString warningString;
};

#endif // TRACKERCLIENT_H
